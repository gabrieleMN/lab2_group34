package com.example.lab2_final.helpers

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.example.lab2_final.R
import com.google.android.material.snackbar.Snackbar
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*

const val DEFAULT_IMAGE = "android.resource://com.example.lab2_final/drawable/profile_demo"
const val DEFAULT_IMAGE_ITEM = "android.resource://com.example.lab2_final/drawable/placeholder"
const val DATE_FORMAT_TIME = "EEE, d MMM ''yy"
const val DEFAULT_IMAGE_URI = "https://firebasestorage.googleapis.com/v0/b/polishop-c10cb.appspot.com/o/images%2Fplaceholder.png?alt=media&token=a3f9e08a-3c71-4ba1-b57b-4239d62f7523"

fun getCurrentDate():String{
    val dateFormat = SimpleDateFormat(DATE_FORMAT_TIME)
    dateFormat.timeZone = TimeZone.getTimeZone("UTC")
    val today = Calendar.getInstance().time
    return dateFormat.format(today)
}

/*  File class extensions
 *
 *  Extensions that provide some additional features to the File class. This
 *  features are copy a stream or a bitmap into the file on which the extension
 *  is called.
 *
 */
fun File.copyInputStreamToFile(inputStream: InputStream) {
    this.outputStream().use { fileOut ->
        inputStream.copyTo(fileOut)
    }
}

fun File.copyBitmapToFile(bitmap: Bitmap){
    val outStream= FileOutputStream(this)
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, FileOutputStream(this))
    outStream.flush()
    outStream.close()
}

/*  MutableLiveData class extensions
 *
 *  Extensions that provide some additional features to the MutableLiveData class.
 *
 */
fun <T> MutableLiveData<ArrayList<T>>.addToList(item:T){
    this.value!!.add(item)
    this.value = value
}

fun <T> MutableLiveData<ArrayList<T>>.addToFrontList(item:T){
    this.value!!.add(0,item)
    this.value = value
}

fun <T> MutableLiveData<ArrayList<T>>.removeFromList(item:T){
    this.value!!.remove(item)
    this.value = value
}

fun <T> MutableLiveData<ArrayList<T>>.updateItem(item:T,index:Int){
    this.value!![index] = item!!
    this.value = this.value
}


/*  CreateImageFile
 *
 *  Create a file inside internal storage /files/images that will contain
 *  the photo shot by the camera
 */
@Throws(IOException::class)
fun createImageFile(context: Context): File {

    val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    val storageDir: File? = File(context.filesDir, "images")
    storageDir!!.mkdir()
    return File.createTempFile("PROFILE_PIC_${timeStamp}_", ".jpg", storageDir)
}


/*  checkAndRotate  LAB SOLUTION
 *
 *  Starting to the image URI, this function returns the corresponding
 *  bitmap in the correct orientation.
 *  It is used to avoid landscape default pic orientation when a
 *  new photo is taken using the camera.
 *  If the path is the default one or if no pic is recognized in the local fs
 *  the profile_demo bitmap is returned
 */
fun checkAndRotate(context: Context, imagePath: String): Bitmap? {
    val ei: ExifInterface?
    val matrix = Matrix()

    if (imagePath == DEFAULT_IMAGE) {
        return BitmapFactory.decodeResource(context.resources, R.drawable.profile_demo)
    }

    try {
        ei = ExifInterface(imagePath)
    } catch (e: IOException) {
        return getBitmapFromPath(imagePath)
    }
    val orientation = ei.getAttributeInt(
        ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)

    val imageBitmap:Bitmap = getBitmapFromPath(imagePath)

    when (orientation) {
        ExifInterface.ORIENTATION_ROTATE_90     -> { matrix.setRotate(90.toFloat()) }
        ExifInterface.ORIENTATION_ROTATE_180    -> { matrix.setRotate(180.toFloat()) }
        ExifInterface.ORIENTATION_ROTATE_270    -> { matrix.setRotate(270.toFloat()) }
        else                                    -> { return null}
    }
    return Bitmap.createBitmap(imageBitmap, 0, 0, imageBitmap.width,
        imageBitmap.height, matrix, true)
}


/* rotate
 *
 * Just rotate the picture, this method is used to fix the unpredictable
 * behaviour of ExiInterface.
 *
 */
fun rotate(imagePath: String, numRotation: Float): Bitmap? {

    val matrix = Matrix()

    if (imagePath == DEFAULT_IMAGE || imagePath == DEFAULT_IMAGE_ITEM) {
        return null
    }

    val imageBitmap:Bitmap = getBitmapFromPath(imagePath)
    matrix.setRotate(90*numRotation)

    return Bitmap.createBitmap(imageBitmap, 0, 0, imageBitmap.width,
        imageBitmap.height, matrix, true)
}


/*  getBitmapFromPath
 *
 * Returns a bitmap given a specific path.
 */
fun getBitmapFromPath(imagePath: String): Bitmap {

    val options = BitmapFactory.Options()
    options.inPreferredConfig = Bitmap.Config.ARGB_8888

    return BitmapFactory.decodeFile(imagePath, options)
}

fun getCurrentDateAndTime(): String{
    val dateFormat = SimpleDateFormat(DATE_FORMAT_TIME)
    dateFormat.timeZone = TimeZone.getTimeZone("UTC")
    val today = Calendar.getInstance().time
    return dateFormat.format(today)
}

fun printEmailNotValid(view: View, context: Context) {
    val snackbar =
        Snackbar.make(view, "Email not valid", Snackbar.LENGTH_LONG)
            .setAction("Action", null)
    val sbView = snackbar.view
    sbView.setBackgroundColor(
        ContextCompat
            .getColor(context, R.color.design_default_color_error))
    snackbar.show()
}

fun printSuccessMessage(view: View, context: Context){
    val snackbar =
        Snackbar.make(view, "Profile updated √", Snackbar.LENGTH_LONG)
            .setAction("Action", null)
    val sbView = snackbar.view
    sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.success));
    snackbar.show()
}

fun printSuccessItemMessage(view: View, context: Context){
    val snackbar =
        Snackbar.make(view, "Item updated √", Snackbar.LENGTH_LONG)
            .setAction("Action", null)
    val sbView = snackbar.view
    sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.success));
    snackbar.show()
}

fun printloginSuccessMessage(view: View, context: Context){
    val snackbar =
        Snackbar.make(view, "Welcome back!", Snackbar.LENGTH_LONG)
            .setAction("Action", null)
    val sbView = snackbar.view
    sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.success));
    snackbar.show()
}

// todo: triggerare questo snackbar quando un item viene venduto correttamente. NON PUOI FARLO NELL'InterestedUserAdapter perché non hai context!
fun printSoldSuccessMessage(view: View, context: Context){
    val snackbar =
        Snackbar.make(view, "Your item has been sold!", Snackbar.LENGTH_LONG)
            .setAction("Action", null)
    val sbView = snackbar.view
    sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.success));
    snackbar.show()
}

fun printErrorLoginMessage(view: View, context: Context){
    val snackbar =
        Snackbar.make(view, "Incorrect username or password.", Snackbar.LENGTH_LONG)
            .setAction("Action", null)
    val sbView = snackbar.view
    sbView.setBackgroundColor(ContextCompat
        .getColor(context, R.color.design_default_color_error))
    snackbar.show()
}

fun printErrorRegisterMessage(view: View, context: Context){
    val snackbar =
        Snackbar.make(view, "User already exist.", Snackbar.LENGTH_LONG)
            .setAction("Action", null)
    val sbView = snackbar.view
    sbView.setBackgroundColor(ContextCompat
        .getColor(context, R.color.design_default_color_error))
    snackbar.show()
}

fun printErrorMessage(view: View, context: Context){
    val snackbar =
        Snackbar.make(view, "Fields cannot be empty", Snackbar.LENGTH_LONG)
            .setAction("Action", null)
    val sbView = snackbar.view
    sbView.setBackgroundColor(ContextCompat
        .getColor(context, R.color.design_default_color_error))
    snackbar.show()
}

fun printFailureSnackbar(view: View, context: Context, message: String) {
    val snackbar =
        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
            .setAction("Action", null)
    val sbView = snackbar.view
    sbView.setBackgroundColor(ContextCompat
        .getColor(context, R.color.design_default_color_error))
    snackbar.show()
}

fun printSuccessSnackbar(view: View, context: Context, message: String){
    val snackbar =
        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
            .setAction("Action", null)
    val sbView = snackbar.view
    sbView.setBackgroundColor(ContextCompat
        .getColor(context, R.color.success))
    snackbar.show()
}
