package com.example.lab2_final.objects

class Notification(
    var title:String = "",
    var date:String ="",
    var message: String = "",
    var itemId: String = "",
    var userInterested: String = "",
    var read:Boolean = false

) {

}