package com.example.lab2_final.objects

class User(
    var fullname: String = "",
    var nickname: String = "",
    var email: String = "",
    var geoArea: String = "",
    var telephone: String = "",
    var password: String = "",
    var token: String = "",
    var imageUrlDownload: String = "https://firebasestorage.googleapis.com/v0/b/polishop-c10cb.appspot.com/o/images%2Fprofile_demo.jpg?alt=media&token=718f0140-354e-427d-a7d7-8812c05595f9",
    var rateReviewTot: Int = 0,
    var nReviews: Int = 0,
    var reviews: ArrayList<Review> = ArrayList()) { //TODO da rimuovere?

    var soldCounter:Int = 0

    fun toHashMap() : HashMap<String, Any>{
        return hashMapOf(
            "fullname" to fullname,
            "nickname" to nickname,
            "email" to email,
            "geoArea" to geoArea,
            "telephone" to telephone,
            "password" to password,
            "token" to token,
            "imageUrlDownload" to imageUrlDownload,
            "rateReviewTot" to rateReviewTot,
            "nReviews" to nReviews,
            "soldCounter" to soldCounter
        )
    }

}