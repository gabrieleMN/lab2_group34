package com.example.lab2_final.objects

class Review(
    var reviewer: String = "",
    var reviewerNickname: String = "",
    var comment: String = "",
    var rate: Int = 0
) {
}