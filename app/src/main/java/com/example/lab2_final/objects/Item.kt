package com.example.lab2_final.objects


data class Item(val name :String = "",
                val category :String = "",
                val subCategory :String = "",
                val description :String = "",
                val location :String = "",
                val city :String = "",
                val expiryDate :String = "",
                val creationDate :String = "",
                val price :String = "0.0",
                val userOwner :String = "",
                var sold :Boolean = false,
                var imageUrlDownlaod :String = "https://firebasestorage.googleapis.com/v0/b/polishop-c10cb.appspot.com/o/images%2Fplaceholder.png?alt=media&token=a3f9e08a-3c71-4ba1-b57b-4239d62f7523",
                var reviewed :Boolean = false,
                var buyer :String = ""){


    var id:String = ""
    var emailInterested: ArrayList<String> = ArrayList<String>()

    fun setItemId(itemId:String){
        id = itemId
    }

    fun toHashMap() : Map<String, Any>{
        return mapOf(
            "name" to name,
            "category" to category,
            "subCategory" to subCategory,
            "description" to description,
            "location" to location,
            "city" to city,
            "expiryDate" to expiryDate,
            "creationDate" to creationDate,
            "price" to price,
            "userOwner" to userOwner,
            "sold" to sold,
            "imageUrlDownlaod" to imageUrlDownlaod,
            "buyer" to buyer
        )
    }

}