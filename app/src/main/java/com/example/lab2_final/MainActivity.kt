package com.example.lab2_final

import android.content.pm.PackageManager
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.lab2_final.helpers.printSuccessSnackbar
import com.example.lab2_final.objects.Notification
import com.example.lab2_final.ui.bought_items.BoughtListViewModel
import com.example.lab2_final.ui.item.ItemViewModel
import com.example.lab2_final.ui.items_of_interest.InterestListViewModel
import com.example.lab2_final.ui.list.ItemListViewModel
import com.example.lab2_final.ui.notification.NotificationListViewModel
import com.example.lab2_final.ui.on_sale.OnSaleListViewModel
import com.example.lab2_final.ui.profile.PERMISSION_REQUEST
import com.example.lab2_final.ui.profile.ProfileViewModel
import com.example.lab2_final.ui.review.ReviewViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.squareup.picasso.Picasso
import com.google.firebase.iid.FirebaseInstanceId
import com.stepstone.apprating.listener.RatingDialogListener
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), RatingDialogListener {

    private lateinit var appBarConfiguration: AppBarConfiguration

    private val itemListVM :ItemListViewModel by viewModels()
    private val profileViewModel :ProfileViewModel by viewModels()
    private val notificationVM :NotificationListViewModel by viewModels()
    private val itemVM :ItemViewModel by viewModels()
    private val reviewViewModel :ReviewViewModel by viewModels()


    //TODO this make rotation at login crash, check if true
    private lateinit var itemInterestObserver: Observer<Notification>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.visibility = View.GONE
        setSupportActionBar(toolbar)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(setOf(
            R.id.nav_item_list,
            R.id.nav_show_profile,
            R.id.nav_on_sale_items,
            R.id.notificationListFragment,
            R.id.fragmentLogin,
            R.id.nav_item_interest,
            R.id.nav_item_bought),
            drawerLayout)

        setupActionBarWithNavController(navController, appBarConfiguration)
        //TODO REMOVE ALL SET CHECK ITEM; EVERY FRAGMENT WILL SET ITSELF AS ITEM CHECKED
        navView.setNavigationItemSelectedListener { menuItem ->
            when(menuItem.itemId){
                R.id.nav_show_profile->{
                    profileViewModel.loadUserInViewModel(profileViewModel.getEmailUserLogged().value!!)
                }
            }
            navController.navigate(menuItem.itemId)
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }

        checkAndRequestPermissions()


        val searchView = findViewById<SearchView>(R.id.searchView)
        searchView.visibility = View.GONE
        val filtersMenu: ConstraintLayout = findViewById(R.id.filtersMenu)
        filtersMenu.visibility = View.GONE

        activateNotifications()

        bellimage.setOnClickListener{
            navController.navigate(R.id.notificationListFragment)
        }

        notificationVM.getNotificationsObservable().observe(this,Observer{

            val notificationBadge = navView.menu.getItem(0).actionView as TextView
            notificationBadge.gravity = Gravity.CENTER_VERTICAL
            notificationBadge.setTypeface(null,Typeface.BOLD)
            notificationBadge.setTextColor(resources.getColor(R.color.accent))
            notificationBadge.text = it.size.toString()

            if(it.isNotEmpty()) {
                bellimage.setImageDrawable(
                    resources.getDrawable(
                        R.drawable.notification,
                        applicationContext.theme
                    )
                )
            }
            else {
                bellimage.setImageDrawable(
                    resources.getDrawable(
                        R.drawable.ic_notification,
                        applicationContext.theme
                    )
                )
            }
        })

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("TAG", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token

                // Log and toast
                Log.d("TAG", token)
            })
    }

    override fun onStart() {
        super.onStart()
        profileViewModel.loggedUserFullname.observe(this, Observer {
            val navigationView : NavigationView  = findViewById(R.id.nav_view)
            val headerView : View = navigationView.getHeaderView(0)
            val navUsername : TextView = headerView.findViewById(R.id.nav_header_user_fullname)
            navUsername.text = it
        })
        profileViewModel.loggedUserEmail.observe(this,Observer<String> {
            val navigationView : NavigationView  = findViewById(R.id.nav_view)
            val headerView : View = navigationView.getHeaderView(0)
            val navEmail : TextView = headerView.findViewById(R.id.nav_header_user_email)
            navEmail.text = it

            if (it != "") {
                reviewViewModel.loadReviews(it)
                toolbar.visibility = View.VISIBLE
            }

        })
        profileViewModel.imgDownloadUrlUserLogged.observe(this, Observer {
            val navigationView : NavigationView = findViewById(R.id.nav_view)
            val headerView : View = navigationView.getHeaderView(0)
            val navImage : CircleImageView = headerView.findViewById(R.id.nav_header_profile_pic)
            Picasso.get().load(it).into(navImage)
        })
        profileViewModel.userEmailVM.observe(this, Observer { currUsrEmail ->
            reviewViewModel.loadReviews(currUsrEmail)
        })
        profileViewModel.loggedUserGeoArea.observe(this, Observer {
            profileViewModel.loggedUserGeoA.value=it
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun checkAndRequestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissions = arrayOf(
                android.Manifest.permission.CAMERA,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)

            val permissionsDenied = arrayListOf<String>()
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(this, permission)
                    == PackageManager.PERMISSION_DENIED
                ) {
                    permissionsDenied.add(permission)
                }
            }

            if (permissionsDenied.isNotEmpty())
                requestPermissions(permissionsDenied.toTypedArray(),
                    PERMISSION_REQUEST
                )
        }
    }


    private fun activateNotifications(){
        profileViewModel.getEmailUserLogged().observe(this, Observer{
            if(it != "") {
                itemListVM.activateInterestNotification()
                notificationVM.activateSoldNotifications()
            }
        })
        activateItemInterestListener()
    }

    private fun activateItemInterestListener() {
       itemInterestObserver = Observer<Notification>{notification ->
           if (notification!= null && !notification.read && notification.message != "") {
               notificationVM.addInterestNotification(notification)
           }
       }
       itemListVM.getNotificationObservable().observe(this,itemInterestObserver)
    }

    override fun onDestroy() {
        super.onDestroy()
        itemListVM.getNotificationObservable().removeObserver(itemInterestObserver)
        itemListVM.setNotificationObservable(null)
    }


    override fun onNegativeButtonClicked() {
        return
    }

    override fun onNeutralButtonClicked() {
        return
    }

    override fun onPositiveButtonClicked(rate: Int, comment: String) {
        Log.d("onPositiveButtonClicked", "rate= $rate comment: $comment itemOwner: ${itemVM.itemOwner.value}")
        // adding a review to the seller
        profileViewModel.addReview(itemVM.nameVM.value!!, rate, comment, itemVM.itemOwner.value!!)

        // setting a review flag for the item to true
        itemVM.reviewed()

        // signaling that the review operations are done correctly
        printSuccessSnackbar(findViewById(android.R.id.content), applicationContext, "Seller has been reviewed")
    }
}
