package com.example.lab2_final.ui.sold_item_interested_users

import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.lab2_final.objects.User
import com.example.lab2_final.ui.item.ItemViewModel
import com.squareup.picasso.Picasso
import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import com.example.lab2_final.R


class InterestedUserAdapter(var items:ArrayList<User>, var itemViewModel: ItemViewModel): RecyclerView.Adapter<InterestedUserAdapter.ViewHolder>() {

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InterestedUserAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.interested_user_card_layout, parent, false)
        return InterestedUserAdapter.ViewHolder(v, itemViewModel)
    }

    override fun onBindViewHolder(holder: InterestedUserAdapter.ViewHolder, position: Int) {
        holder.bind(items[position])
    }
    override fun onViewRecycled(holder: ViewHolder) {
        holder.unbind()
    }

    class ViewHolder(v: View, itemViewModel: ItemViewModel): RecyclerView.ViewHolder(v) {
        val itmVM = itemViewModel
        val user: ConstraintLayout = v.findViewById(R.id.user)
        val name: TextView = v.findViewById(R.id.user_nickname)
        val photo: ImageView = v.findViewById(R.id.user_img)
        private val ctx: Context = v.context
        fun bind(u: User) {
            name.text = u.nickname
            //photo.setImageURI(Uri.parse(i.imagePath))
            user.setOnClickListener {
                // qui bisogna passare l'utente che è stato selezionato alla markItemAsSold
                // in modo da aggiornare correttamente l'utente. Questo dovrà poi esprimere un
                // rating sul seller dopo che questo avrà venduto lui l'item.
                // successivamente bisognerà aggiornare:
                //      numero totale di recensioni del seller (++)
                //      aumentare valore rating con quello dell'item appena venduto ->
                //          actual_rating_tot += new_rating_value_per_obj
                //      aggiornare la vista rating per l'utente che ha ricevuto il rating.
                //
                // guarda markItemAsSold dentro showDIalogConfirm
                showDialogConfirm(ctx, u.email, it)
            }
            Picasso.get().load(u.imageUrlDownload).into(photo)
        }
        fun unbind(){
            user.setOnClickListener(null)
        }

        private fun showDialogConfirm(context :Context, mail: String, v: View) {
            // set title
            val alertDialogBuilder = AlertDialog.Builder(context)
            alertDialogBuilder.setTitle("Attention")
            alertDialogBuilder.setIcon(R.drawable.logitipo_no_background)

            // set dialog message
            alertDialogBuilder
                .setMessage("do you want to sell the item to this user?")
                .setCancelable(false)
                .setPositiveButton("sell",
                    DialogInterface.OnClickListener { _, _ -> // if this button is clicked, close
                        Log.d("showDialogConfirm", "confirm")
                        itmVM.markItemAsSold(mail)
                        val bundle = Bundle()
                        bundle.putBoolean("showInterestedUsers",false)
                        Navigation.findNavController(v).navigate(R.id.action_soldItemInterestedUsers_to_nav_item_details, bundle)
                    })
                .setNegativeButton("cancel",
                    DialogInterface.OnClickListener { dialog, _ -> // if this button is clicked, just close
                        Log.d("showDialogConfirm", "cancel")
                        dialog.cancel()
                    })

            // create alert dialog
            val alertDialog: AlertDialog = alertDialogBuilder.create()

            // show it
            alertDialog.show()

            alertDialog.findViewById<Button>(android.R.id.button1).setBackgroundColor(Color.parseColor("#FAFAFA"))
            alertDialog.findViewById<Button>(android.R.id.button1).setTextColor(Color.parseColor("#212121"))
            alertDialog.findViewById<Button>(android.R.id.button2).setBackgroundColor(Color.parseColor("#FAFAFA"))
            alertDialog.findViewById<Button>(android.R.id.button2).setTextColor(Color.parseColor("#212121"))

        }

    }

}