package com.example.lab2_final.ui.profile

import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.lab2_final.objects.User
import com.google.android.gms.tasks.OnCompleteListener
import com.google.common.io.Files.getFileExtension
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class ProfileRepository {

    fun getUser(usrID: String) :MutableLiveData<User> {
        val data = MutableLiveData<User>()
        val db = Firebase.firestore
        var usrObj: User?

        Log.d("getUser", "sono entrato in getUser in ProfileRepository")
        db
        .collection("utenti")
        .document(usrID)
        .get()
        .addOnSuccessListener { res ->
            if (res.exists()) {
                usrObj = res.toObject(User::class.java)
                data.value = usrObj
                Log.d("updatingUser","userDB: "+usrObj?.fullname)
                Log.d("updatingUser","userVM: "+data.value?.fullname)
            }
            //use it as needed
        }
        .addOnFailureListener {

            Log.d("getUser", "db failure")
        }

        return data
    }

    fun updateUser(usrObj: User,userId:MutableLiveData<String>,loggedUserEmail:MutableLiveData<String>,photoUri: Uri) {
        val db = Firebase.firestore
        val auth = Firebase.auth

        db
        .collection("utenti")
        .document(usrObj.email)
                //TODO chiedere se bastano questi campi
        .set( usrObj.toHashMap(), SetOptions.mergeFields("fullname","geoArea","nickname","password","telephone") )
        .addOnSuccessListener {
            //userId.value = userId.value
            auth.currentUser?.updatePassword(usrObj.password)
            uploadImage(photoUri,loggedUserEmail,userId)
            Log.d("updateUser", "user updated")
        }
        .addOnFailureListener {
            Log.d("updateUser", "user NOT updated")
        }

    }

    fun getUserList(emails:List<String>):MutableLiveData<ArrayList<User>>{
        val utenti:MutableLiveData<ArrayList<User>> = MutableLiveData(ArrayList())
        val db = Firebase.firestore
        var user:User =User()
        for(i in emails.indices) {
            db.collection("utenti")
                .whereEqualTo("email",emails[i])
                .get()
                .addOnSuccessListener { documents ->
                    for (document in documents) {
                        user= document.toObject(User::class.java)
                        utenti.value?.add(user)
                    }
                    if(i==emails.size-1)
                        utenti.value=utenti.value

                }
                .addOnFailureListener {
                }
        }

        return utenti
    }

    fun login(mail: String,
              emailUserLogged: MutableLiveData<String>,
              loginFailureFlag: MutableLiveData<Boolean>) {
        val db = Firebase.firestore

        emailUserLogged.value = mail
        loginFailureFlag.value = false
        Log.d("login_repo", "res.exists login done. Email-> " + mail)

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener(
            OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                val token = task.result?.token!!
                db
                    .collection("utenti")
                    .document(mail)
                    .update("token", token)
            })
    }


    fun uploadImage(fileUri: Uri, email: MutableLiveData<String>, userId :MutableLiveData<String>) {
        val imageReference = FirebaseStorage.getInstance().reference.child("images").child(email.value!!).child("profile")
        val fileRef = imageReference.child("profilePic" + "." + getFileExtension(fileUri.toString()))
        val uploadTask = fileRef.putFile(fileUri)

        val urlTask = uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let { throw it }
            }
            fileRef.downloadUrl
        }.addOnCompleteListener{ task ->
            if(task.isSuccessful){
                //Mi salvo l'url di download
                Log.d("URL", task.result.toString())
                val imgDownloadUrl = task.result.toString()

                val db = Firebase.firestore
                db.collection("utenti")
                .document(email.value!!)
                .update("imageUrlDownload",imgDownloadUrl)
                .addOnSuccessListener {
                    userId.value = userId.value
                    email.value = email.value
                }
            }
        }
    }


    fun register(mail: String, pwd: String,
                 emailUserLogged: MutableLiveData<String>,
                 flagUserExist: MutableLiveData<Boolean>) {
        val db = Firebase.firestore
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                return@OnCompleteListener
            }
            // Get new Instance ID token
            val token = task.result?.token!!
            Log.d("register_repository", token)

            val usr: User = User("", "", mail,
                "", "", pwd, token )
            db
            .collection("utenti")
            .document(mail)
            .get()
            .addOnSuccessListener { res ->
                if (!res.exists()) {
                    Log.d("register_repository", "user not exist")
                    db
                    .collection("utenti")
                    .document(mail)
                    .set( usr.toHashMap() )
                    .addOnSuccessListener {
                        Log.d("register_repository", "User registered")
                        emailUserLogged.value = mail
                        flagUserExist.value = false
                    }
                    .addOnFailureListener {
                        Log.d("register_repository", "db failure")
                        emailUserLogged.value = ""
                    }
                } else {
                    Log.d("register_repository", "User NOT registered, already exist")
                    flagUserExist.value = true
                }
            }
            .addOnFailureListener {
                Log.d("register_repository", "db failure")
            }
        })
    }

    fun loadImageIntoFile(userEmail: String, currentPhotoFile: MutableLiveData<File>){
        val imageReference = FirebaseStorage.getInstance().reference.child("images").child(userEmail).child("profile")
        val fileRef = imageReference.child("profilePic" + "." + "jpg")
        val file = createTempFile("img","jpg")

        fileRef.getFile(file).addOnSuccessListener {
            // Local temp file has been created
            Log.d("PhotoFile","Success")
            currentPhotoFile.value = file
        }.addOnFailureListener {
            // Handle any errors
            Log.d("PhotoFile","Failure")
        }

    }

    fun initImageIntoFile() :MutableLiveData<File> {
        val dataFile = MutableLiveData<File>()
        val imageReference = FirebaseStorage.getInstance().reference.child("images")
        val imgRef = imageReference.child("profile_demo.jpg")
        val file = createTempFile("img_init","jpg")

        imgRef.getFile(file).addOnSuccessListener {
            dataFile.value = file
        }
        return dataFile
    }

    /* thanks by this function its possible add a review to a seller */
    fun addReview(itemReviewed :String, reviewerEmail :String, reviewerNickname: String, rate :Int,
                  comment :String, sellerEmail :String) {
        val db = Firebase.firestore

        val hmap = hashMapOf(
            "reviewer" to reviewerEmail,
            "reviewerNickname" to reviewerNickname,
            "rate" to rate,
            "comment" to comment)

        // adding a new review for the seller user
        db
        .collection("utenti")
        .document(sellerEmail)
        .collection("reviews")
        .document(itemReviewed)
        .set(hmap as Map<String, Any>)
        .addOnSuccessListener {

            // incrementing number of total rate
            db
            .collection("utenti")
            .document(sellerEmail)
            .update( "rateReviewTot", FieldValue.increment(rate.toDouble()) )

            // incrementing number of total reviews
            db
            .collection("utenti")
            .document(sellerEmail)
            .update("nReviews", FieldValue.increment(1))
        }
    }

    companion object {
        private var INSTANCE: ProfileRepository? = null
        fun getInstance() = INSTANCE
            ?: ProfileRepository().also {
                INSTANCE = it
            }
    }
}

/*
    fun getLoggedUser(usrID: String, user: MutableLiveData<User>){
        val db = Firebase.firestore

        Log.d("getUser", "sono entrato in getUser in ProfileRepository")
        db
            .collection("utenti")
            .document(usrID)
            .get()
            .addOnSuccessListener { res ->
                if (res.exists()) {
                    user.value = res.toObject(User::class.java)
                    Log.d("getUser-in", user.value!!.fullname)
                }
                //use it as needed
            }
            .addOnFailureListener {

                Log.d("getUser", "db failure")
            }
    }

    fun getUser(usrID: String, onResult:(isSuccess:Boolean, response:DocumentSnapshot)-> Unit)  {
        val db = Firebase.firestore
        val data = MutableLiveData<User>()
        var usrObj : User? = User()

        Log.d("getUser", "sono entrato in getUser in ProfileRepository")

        db
        .collection("utenti")
        .document(usrID)
        .get()
        .addOnSuccessListener { res ->
            if (res.exists()) {
                onResult(true,res)
                Log.d("getUser-in", usrObj!!.fullname)
            }
            //use it as needed
        }
        .addOnFailureListener {

            Log.d("getUser", "db failure")
        }

        return data
    }

     */
