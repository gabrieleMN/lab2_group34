package com.example.lab2_final.ui.items_of_interest

import androidx.lifecycle.MutableLiveData

import com.example.lab2_final.helpers.addToList
import com.example.lab2_final.helpers.removeFromList
import com.example.lab2_final.objects.Item
import com.google.firebase.auth.ktx.auth

import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class InterestListRepository {

    fun getItemsOfInterest():MutableLiveData<ArrayList<Item>>{

        val itemsOfInterest = MutableLiveData(ArrayList<Item>())
        val db = Firebase.firestore

        db
            .collection("utenti")
            .document(Firebase.auth.currentUser!!.email!!)
            .collection("items")
            .whereEqualTo("sold",false)
            /* These are only the id of the item the user is interested in. For every item we
            *  need another query that get from the db all the info of the items. For every id
            *  that is added we attach a snapshot listener to the new item the user is interested
            *  in in order to catch remote changes. */
            .addSnapshotListener { value, _ ->
                for (dc in value!!.documentChanges) {
                    when (dc.type) {

                        /* If the user in interested in a new item, add it to the favorites local
                        *  list. */
                        DocumentChange.Type.ADDED -> {
                           getItemFromId(itemsOfInterest,dc.document.id)
                        }

                        /* This entry could change if the flag READ change remotly.
                        *  If the flag SOLD become true it is considered a REMOVED because the query
                        *  on which the snapshot listener is attached is where sold == false .*/
                        DocumentChange.Type.MODIFIED -> {

                        }

                        /* The user is no more interested in the in the item or the item has been sold */
                        DocumentChange.Type.REMOVED -> {
                            val index = itemsOfInterest.value!!.indexOfFirst { it.id == dc.document.id }
                            val i = itemsOfInterest.value!![index]
                           itemsOfInterest.removeFromList(i)
                        }

                    }
                }
            }
        return itemsOfInterest
    }

    private fun getItemFromId(itemsOfInterest:MutableLiveData<ArrayList<Item>>, itemId:String){
        val db = Firebase.firestore
        db
            .collection("items")
            .document(itemId)
            .addSnapshotListener { snapshot, e ->

                if (e != null) {
                    return@addSnapshotListener}

                if (snapshot != null && snapshot.exists()) {
                    var item = snapshot.toObject(Item::class.java)
                    item!!.setItemId(snapshot.id)

                    /*  Here i have a snapshot listener directly on a document so i can't use snapshotDiff
                     *  in order to check if it is an ADD on a MODIFIED. Every time the listener is fired
                     *  i check if i already have that unique itemId inside my local list. If true it means
                     *  that the item has been modified remotely otherwise is a new item.
                     */

                    val index = itemsOfInterest.value!!.indexOfFirst { it.id == itemId }

                    if(index==-1){
                        //new favourite item
                        itemsOfInterest.addToList(item!!)
                    }else{
                        //one of my favourite item has been changed remotly
                        itemsOfInterest.value!![index] = item!!
                        itemsOfInterest.value = itemsOfInterest.value
                    }
                }
            }
    }


    companion object {
        private var INSTANCE: InterestListRepository? = null
        fun getInstance() = INSTANCE
            ?: InterestListRepository().also {
                INSTANCE = it
            }
    }
}