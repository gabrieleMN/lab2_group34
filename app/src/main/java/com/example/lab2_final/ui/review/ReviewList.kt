package com.example.lab2_final.ui.review

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lab2_final.R
import com.example.lab2_final.ui.profile.ProfileViewModel
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.fragment_review_list.*
import kotlinx.android.synthetic.main.item_list_fragment.*

class ReviewList: Fragment() {

    private lateinit var reviewAdapter :ReviewAdapter
    private lateinit var reviewViewModel :ReviewViewModel
    private lateinit var profileViewModel :ProfileViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        reviewViewModel = ViewModelProvider(requireActivity()).get(ReviewViewModel::class.java)
        profileViewModel = ViewModelProvider(requireActivity()).get(ProfileViewModel::class.java)
//        reviewViewModel.loadReviews(profileViewModel.userEmailVM.value!!)
        reviewAdapter = ReviewAdapter(reviewViewModel.getReviewsArrayList())
    }

    override fun onStart() {
        super.onStart()
        profileViewModel.userEmailVM.observe(viewLifecycleOwner, Observer { userEmail ->
            reviewViewModel.loadReviews(userEmail)
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_review_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.adapter = reviewAdapter

        recyclerView.layoutManager = LinearLayoutManager(context)


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
    }

}