package com.example.lab2_final.ui.profile

import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.lab2_final.R
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_show_profile.*
import kotlinx.android.synthetic.main.fragment_show_profile.label_fullname
import kotlinx.android.synthetic.main.fragment_show_profile.label_password
import kotlinx.android.synthetic.main.fragment_show_profile.label_telephone

const val PERMISSION_REQUEST = 10
const val PREF_NAME = "USER"
const val PREF_KEY = "profile"


class ShowProfileFragment : Fragment(),OnMapReadyCallback {
    //private lateinit var profileModel : ProfileViewModel
    private val profileModel: ProfileViewModel by activityViewModels()

     var googleMap: GoogleMap? = null
     var mapView: MapView? = null
    private var mapsSupported = true

    override fun onStart() {
        super.onStart()

        registerObserversShowProfile(profileModel)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    findNavController().navigate(R.id.nav_item_list)
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val parent =
            inflater.inflate(R.layout.fragment_show_profile, container, false)
        mapView = parent.findViewById(R.id.mapView) as MapView
        mapView!!.getMapAsync(this)
        return parent
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(!profileModel.isCurrentUser()){
            this.label_password.visibility = View.GONE
            this.password_textView.visibility = View.GONE
        }

        reviews.setOnClickListener{
            findNavController().navigate(R.id.action_nav_show_profile_to_reviewList2)
        }

        val imagebell = requireActivity().findViewById<ImageView>(R.id.bellimage)
        imagebell.visibility=View.VISIBLE
    }


    // This method should be triggered inside the onclick listener of the edit button.
    private fun editProfile() {
        val navig = findNavController()
        navig.navigate(R.id.action_nav_show_profile_to_nav_edit_profile)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.layout_options_menu1, menu)
        if (!profileModel.isCurrentUser())
            menu.removeItem(R.id.edit)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.edit -> {
                editProfile()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
       super.onCreate(savedInstanceState)
        try {
            MapsInitializer.initialize(getActivity())
        } catch (e: GooglePlayServicesNotAvailableException) {
            mapsSupported = false
        }
        if (mapView != null) {
            mapView!!.onCreate(savedInstanceState)
        }
        initializeMap()
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
        requireActivity().findViewById<NavigationView>(R.id.nav_view).setCheckedItem(R.id.nav_show_profile)
    }

    //Function that register all the observers
    //If useful, move to Helpers
    fun registerObserversShowProfile(profileVM: ProfileViewModel){
        profileVM.userPasswordVM.observe(viewLifecycleOwner, Observer { userPassword -> this.password_textView.text = userPassword })
        profileVM.userNameVM.observe(viewLifecycleOwner, Observer { userName -> Log.d("updatingUser",
            "User updated in show profile $userName"
        )
            this.fullName_textView.text = userName })
        profileVM.userNicknameVM.observe(viewLifecycleOwner, Observer { userNickname -> this.nickname_textView.text = userNickname })
        profileVM.userEmailVM.observe(viewLifecycleOwner, Observer { userEmail -> this.email_textView.text = userEmail })
        profileVM.userGeoAreaVM.observe(viewLifecycleOwner, Observer { userGeoArea -> this.geoArea_textView.text = userGeoArea
        mapView!!.getMapAsync(this)
        })
        profileVM.userTelephoneVM.observe(viewLifecycleOwner, Observer { userTel -> this.telephone_textView.text = userTel })
        profileVM.userRateReviewTot.observe(viewLifecycleOwner, Observer { rateReviewTot ->
            if (profileVM.userNReviews.value != null && profileVM.userNReviews.value!! != 0) {
                rating_textview.text = (rateReviewTot.toFloat() / profileVM.userNReviews.value!!.toFloat()).toString()
            }
        })
        profileVM.userNReviews.observe(viewLifecycleOwner, Observer { nReviews ->
            if (profileVM.userRateReviewTot.value != null && profileVM.userRateReviewTot.value!! != 0)
                rating_textview.text = (profileVM.userRateReviewTot.value!!.toFloat() / nReviews.toFloat()).toString()
        })

        profileVM.userSoldCounter.observe(viewLifecycleOwner, Observer { soldCounter ->
            num_sold_textview.text = soldCounter.toString()
        })

        profileVM.imgDownloadUrl.observe(viewLifecycleOwner, Observer { imgDownloadUrl -> Picasso.get().load(imgDownloadUrl).placeholder(R.drawable.profile_demo).into(profilePic) }) }

override fun onResume() {
    super.onResume()
    mapView!!.onResume()
    initializeMap()
}

    override fun onMapReady(p0: GoogleMap?) {
        googleMap=p0


        var address: Address? =null
        val location = geoArea_textView.text.toString()
        var addressList: List<Address>? = null
        if (location != null || location != "") {
            val geocoder = Geocoder(requireActivity())
            try {
                addressList = geocoder.getFromLocationName(location, 1)
                address = addressList!![0]

            } catch (e: Exception) {
                e.printStackTrace()
                 return;
            }
            googleMap?.clear()
            val latLng = LatLng(address!!.getLatitude(), address!!.getLongitude())
            googleMap!!.addMarker(MarkerOptions().position(latLng))
            googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15.0F))
        }

    }
    private fun initializeMap() {
        if (googleMap == null && mapsSupported) {
            mapView = getActivity()?.findViewById(R.id.mapView)
        }
    }

}

