package com.example.lab2_final.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.lab2_final.R
import com.example.lab2_final.objects.Item
import com.example.lab2_final.ui.item.ItemViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso

class ItemAdapter(var items:ArrayList<Item>,var itemViewModel: ItemViewModel): RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
    val itemVM:ItemViewModel = itemViewModel

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_layout, parent, false)
        return ViewHolder(v, itemVM)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun onViewRecycled(holder: ViewHolder) {
        holder.unbind()
    }

    class ViewHolder(v: View, itemViewModel: ItemViewModel) : RecyclerView.ViewHolder(v) {
        val itemVM = itemViewModel
        val item: ConstraintLayout= v.findViewById(R.id.item)
        val name: TextView = v.findViewById(R.id.item_name)
        val price: TextView = v.findViewById(R.id.item_price)
        val category: TextView = v.findViewById(R.id.item_category)
        val btn_editItem: ImageButton = v.findViewById(R.id.btn_editItem)
        val photo: ImageView = v.findViewById(R.id.item_pic)
        val textViewSold:TextView = v.findViewById(R.id.soldTextView)
        fun bind(i: Item) {
            name.text = i.name
            price.text = i.price.toString()+"€"
            category.text = i.category

            if( Firebase.auth.currentUser!!.email!! == i.userOwner && !i.sold){
                    btn_editItem.visibility = View.VISIBLE
                    btn_editItem.setOnClickListener {
                        val bundle = Bundle()
                        itemVM.loadItemInViewModel(i.id)
                        Navigation.findNavController(it)
                            .navigate(R.id.action_nav_item_list_to_itemEditFragment, bundle)
                    }
                item.setOnClickListener {
                    val bundle = Bundle()
                    itemVM.loadItemInViewModel(i.id)
                    bundle.putBoolean("showInterestedUsers",true)
                    Navigation.findNavController(it).navigate(R.id.nav_item_details,bundle)
                }
            }else {
                btn_editItem.visibility = View.GONE
                item.setOnClickListener {
                    val bundle = Bundle()
                    itemVM.loadItemInViewModel(i.id)
                    bundle.putBoolean("showInterestedUsers",false)
                    Navigation.findNavController(it).navigate(R.id.nav_item_details,bundle)
                }
            }

            if (!i.sold)
                textViewSold.visibility = View.GONE
            else{
                textViewSold.visibility = View.VISIBLE
            }


            Picasso.get().load(i.imageUrlDownlaod).placeholder(R.drawable.placeholder).into(photo)
        }
        fun unbind(){
            btn_editItem.setOnClickListener(null)
            item.setOnClickListener(null)
        }

    }

}

/* To use in case of NotifyDataSetChanged

fun set_items(newItems:Array<Item>){
    val diffs = DiffUtil.calculateDiff(ItemDiffCallbacks(items,newItems))
    items = newItems
    diffs.dispatchUpdatesTo(this)
}
class ItemDiffCallbacks(private val items: Array<Item>, private val newItems: Array<Item>) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = items.size
        override fun getNewListSize(): Int = newItems.size
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return items[oldItemPosition] == newItems[newItemPosition]
        }
        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val (name,category,price) = items[oldItemPosition]
            var (name1, category1, price1 ) = newItems[newItemPosition]
            return name==name1 && category==category1 && price==price1
        }

} */