package com.example.lab2_final.ui.bought_items

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.lab2_final.objects.Item

class BoughtListViewModel: ViewModel() {

    private var boughItems = BoughtListRepository.getInstance().getBoughtItems()

    fun getItemsObservable() : LiveData<ArrayList<Item>> {
        return boughItems
    }
}