package com.example.lab2_final.ui.on_sale

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lab2_final.objects.Item
import com.example.lab2_final.objects.Notification

class OnSaleListViewModel: ViewModel() {

    private var itemsOnSale = OnSaleListRepository.getInstance().getItemsOnSale()

    fun getItemsObservable() : LiveData<ArrayList<Item>> {
        return itemsOnSale
    }

}
