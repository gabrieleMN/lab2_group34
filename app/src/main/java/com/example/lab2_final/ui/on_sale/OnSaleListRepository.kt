package com.example.lab2_final.ui.on_sale

import androidx.lifecycle.MutableLiveData

import com.example.lab2_final.helpers.addToList
import com.example.lab2_final.helpers.removeFromList
import com.example.lab2_final.helpers.updateItem
import com.example.lab2_final.objects.Item
import com.google.firebase.auth.ktx.auth

import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class OnSaleListRepository {

    fun getItemsOnSale() : MutableLiveData<ArrayList<Item>> {
        val itemsOnSale = MutableLiveData<ArrayList<Item>>(ArrayList<Item>())

        val db = Firebase.firestore

        db
            .collection("items")
            .whereGreaterThan("userOwner", Firebase.auth.currentUser!!.email!!)
            .whereEqualTo("sold",false)
            .addSnapshotListener { value, e ->
                    for (dc in value!!.documentChanges) {
                        var item = dc.document.toObject(Item::class.java)
                        manageRealTimeChanges(item!!, itemsOnSale, dc)
                    }
                itemsOnSale.value = itemsOnSale.value
            }

        db
            .collection("items")
            .whereLessThan("userOwner",  Firebase.auth.currentUser!!.email!!)
            .whereEqualTo("sold",false)
            .addSnapshotListener { value, e ->
                    for (dc in value!!.documentChanges) {
                        var item = dc.document.toObject(Item::class.java)
                        manageRealTimeChanges(item!!, itemsOnSale, dc)
                    }
            }

        return itemsOnSale
    }

    private fun manageRealTimeChanges(item:Item, itemsOnSale:MutableLiveData<ArrayList<Item>>, dc:DocumentChange){
        /* TODO : SnapShotListener fired double for every modification
           This probably make the snapshot being fired again because it's a local modification,
           we must distinguish local from remote?
         */
        item!!.setItemId(dc.document.id)

        when(dc.type) {
            DocumentChange.Type.ADDED -> {
                itemsOnSale.addToList(item!!)
            }

            DocumentChange.Type.MODIFIED -> {
                var index = itemsOnSale.value!!.indexOfFirst { it.id == dc.document.id }
                /* index == -1 if the subcollection emails of an item is modified, because the document
                 * id is referred to the mail of the user that is interested in it and not to the item
                 * id. Discard this kind of changes here, we are not interested in them.
                 */
                if (index != -1)
                    itemsOnSale.updateItem(item,index)
            }
            DocumentChange.Type.REMOVED -> {
                /* Since the query on which we add the snasphot listener is where sold = false when an item
                 * is sold is removed from the result of this query.
                 */
                itemsOnSale.removeFromList(item)
            }
        }
    }

    companion object {
        private var INSTANCE: OnSaleListRepository? = null
        fun getInstance() = INSTANCE
            ?: OnSaleListRepository().also {
                INSTANCE = it
            }
    }

}