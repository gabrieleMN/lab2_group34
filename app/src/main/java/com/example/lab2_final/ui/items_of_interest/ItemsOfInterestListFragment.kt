package com.example.lab2_final.ui.items_of_interest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lab2_final.R
import com.example.lab2_final.ui.item.ItemViewModel
import com.example.lab2_final.ui.list.ItemAdapter
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.item_list_fragment.*
import kotlinx.android.synthetic.main.notifications_list_fragment.*
import kotlinx.android.synthetic.main.notifications_list_fragment.empty_list
import kotlinx.android.synthetic.main.on_sale_list_fragment.*

class ItemsOfInterestListFragment:Fragment() {

    private lateinit var itemAdapter: ItemAdapter
    private lateinit var itemViewModel: ItemViewModel
    private lateinit var itemsOfInterestVM: InterestListViewModel

    override fun onStart() {
        super.onStart()

        itemsOfInterestVM.getItemsObservable().observe(viewLifecycleOwner, Observer { arrayList ->
            itemAdapter.items = arrayList
            itemAdapter.notifyDataSetChanged()

            empty_list_action.setOnClickListener {
                Navigation.findNavController(it).navigate(R.id.nav_on_sale_items)
            }

            if(itemsOfInterestVM.getItemsObservable().value!!.size == 0) {
                empty_list.visibility = View.VISIBLE
                empty_list_action.visibility = View.VISIBLE
                listTitle.visibility = View.GONE
            }else{
                empty_list.visibility = View.GONE
                empty_list_action.visibility = View.GONE
                listTitle.visibility = View.VISIBLE
            }
        })


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        itemsOfInterestVM = ViewModelProvider(requireActivity()).get(InterestListViewModel::class.java)
        itemViewModel = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        itemAdapter = ItemAdapter(itemsOfInterestVM.getItemsObservable().value!!, itemViewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.favourites_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //set the adapter and layoutManager to the recycler view
        recyclerView.adapter = itemAdapter

        //set the layout manager, should only be set once
        recyclerView.layoutManager = LinearLayoutManager(context)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
        requireActivity().findViewById<NavigationView>(R.id.nav_view).setCheckedItem(R.id.nav_item_interest)

    }
}