package com.example.lab2_final.ui.on_sale

import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lab2_final.R
import com.example.lab2_final.ui.item.ItemViewModel
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.on_sale_list_fragment.*

class OnSaleListFragment: Fragment() {

    private lateinit var itemAdapter: OnSaleItemAdapter
    private lateinit var itemOnSaleVM : OnSaleListViewModel
    private lateinit var itemVM: ItemViewModel

    private var activeFiltersCount:Int = 0
    private var activeFilters = ArrayList<Int>()                //management of the filters ID
    private var activeFiltersType = HashMap<String,String>()    //management of the filters type

    override fun onStart() {
        super.onStart()
        itemOnSaleVM.getItemsObservable().observe(viewLifecycleOwner, Observer { arrayList->
            itemAdapter.items = arrayList
            itemAdapter.notifyDataSetChanged()
            /* restore filtering constraints */
            if(activeFiltersType.size > 0)
                restoreFilters()

            requireActivity().findViewById<TextView>(R.id.nResult).text = itemAdapter.itemCount.toString()
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        itemOnSaleVM = ViewModelProvider(requireActivity()).get(OnSaleListViewModel::class.java)
        itemVM = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        itemAdapter = OnSaleItemAdapter(itemOnSaleVM.getItemsObservable().value!!, itemVM)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.on_sale_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //set the adapter and layoutManager to the recycler view
        recyclerView.adapter = itemAdapter

        //set the layout manager, should only be set once
        recyclerView.layoutManager = LinearLayoutManager(context)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
        requireActivity().findViewById<NavigationView>(R.id.nav_view).setCheckedItem(R.id.nav_on_sale_items)
        //restoring local variable after rotation
        if(savedInstanceState != null){
            savedInstanceState.getIntegerArrayList("filters")?.let { activeFilters.addAll(it) }
            activeFiltersCount = activeFilters.size
            activeFiltersType = savedInstanceState.getSerializable("filtersType") as HashMap<String, String>
        }

        val searchView = requireActivity().findViewById<SearchView>(R.id.searchView)
        if (searchView.visibility == View.GONE)
            searchView.visibility = View.VISIBLE

        val filtersMenu:ConstraintLayout = requireActivity().findViewById(R.id.filtersMenu)
        if (filtersMenu.visibility == View.GONE)
            filtersMenu.visibility = View.VISIBLE

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                if(newText=="")
                    restoreFilters()
                itemAdapter.filter.filter(newText)
                return false
            }
        })

        val btnFilter = requireActivity().findViewById<Button>(R.id.btn_filter)
        btnFilter.setOnClickListener {
            val filtersScroll = requireActivity().findViewById<NestedScrollView>(R.id.filtersScroll)
            val iconDraw:Drawable?

            if (filtersScroll.isVisible) {
                filtersScroll.visibility = View.GONE
                iconDraw = requireActivity().resources.getDrawable(R.drawable.ic_arrow_drop_down_black_24dp)
                (it as Button).setBackgroundColor(resources.getColor(R.color.icons))
            }
            else {
                filtersScroll.visibility = View.VISIBLE
                iconDraw = requireActivity().resources.getDrawable(R.drawable.ic_arrow_drop_up_black_24dp)
                (it as Button).setBackgroundColor(resources.getColor(R.color.colorAccentBack))
            }

            it.setCompoundDrawablesWithIntrinsicBounds(null,null,iconDraw,null)
        }

        val pricesFilter = requireActivity().findViewById<ChipGroup>(R.id.prices_filter)
        for (index in 0 until pricesFilter.childCount) {
            val nextChild = pricesFilter.getChildAt(index) as Chip
            nextChild.setOnCheckedChangeListener { chip, isChecked ->
                manageFilter(chip,isChecked,"price")
            }

        }

        val categoriesFilter = requireActivity().findViewById<ChipGroup>(R.id.categories_filter)
        for (index in 0 until categoriesFilter.childCount) {
            val nextChild = categoriesFilter.getChildAt(index) as Chip
            nextChild.setOnCheckedChangeListener { chip, isChecked ->
               manageFilter(chip,isChecked,"category")
            }

        }

        val sortAsc = requireActivity().findViewById<Chip>(R.id.sort_asc)
        sortAsc.setOnCheckedChangeListener { chip, isChecked ->
            manageSort(chip,isChecked,"sortingAsc","asc")
        }

        val sortDesc = requireActivity().findViewById<Chip>(R.id.sort_desc)
        sortDesc.setOnCheckedChangeListener { chip, isChecked ->
            manageSort(chip,isChecked,"sortingDesc","desc")
        }

        val imagebell = requireActivity().findViewById<ImageView>(R.id.bellimage)
        imagebell.visibility=View.VISIBLE

    }

    override fun onStop() {
        super.onStop()

        /* When we switch to another fragment reset the filter */
        val toUncheck = activeFilters.toTypedArray()
        for(chipId in toUncheck)
            requireActivity().findViewById<Chip>(chipId).isChecked=false

        val navView: NavigationView = requireActivity().findViewById(R.id.nav_view)

        val filtersDialog = requireActivity().findViewById<NestedScrollView>(R.id.filtersScroll)
        if(filtersDialog.isVisible) {
            filtersDialog.visibility = View.GONE
            val btnFilter = requireActivity().findViewById<Button>(R.id.btn_filter)
            val iconDraw = requireActivity().resources.getDrawable(R.drawable.ic_arrow_drop_down_black_24dp)
            btnFilter.setBackgroundColor(resources.getColor(R.color.icons))
            btnFilter.setCompoundDrawablesWithIntrinsicBounds(null,null,iconDraw,null)
        }
        /* This solve a BUG related to the filter.*/
        if(navView.checkedItem.toString() != "Market") {
            /* Also hide the view realted to the filters */
            val searchView: SearchView = requireActivity().findViewById(R.id.searchView)
            searchView.visibility = View.GONE
            val filtersMenu: ConstraintLayout = requireActivity().findViewById(R.id.filtersMenu)
            filtersMenu.visibility = View.GONE
        }

    }

    private fun restoreFilters(){

        var resultSize:Int = 0

        /* Restore filters count*/
        val btnFilter = requireActivity().findViewById<Button>(R.id.btn_filter)
        if(activeFiltersCount > 0)
            btnFilter.text = "Filter (" + activeFiltersCount.toString() + ")"
        else
            btnFilter.text = "Filter"

        resultSize = itemAdapter.performFilter("")

        val searchText = requireActivity().findViewById<SearchView>(R.id.searchView).query.toString()
        if(searchText.isNotEmpty())
            itemAdapter.filter.filter(searchText)

        /* Restore filters */
        activeFiltersType.forEach{
            if(it.key == "sortingAsc" || it.key == "sortingDesc" )
                itemAdapter.sortItems(it.value)
            else {
                itemAdapter.filterType = it.value
                resultSize = itemAdapter.performFilter(it.key)
            }
        }

        requireActivity().findViewById<TextView>(R.id.nResult).text = resultSize.toString()

    }

    private fun manageFilter(chip: CompoundButton, isChecked:Boolean, filterType:String){
        val chipView = chip as Chip
        if (isChecked) {
            chipView.chipStrokeColor = ColorStateList.valueOf(
                ContextCompat.getColor(requireActivity(), R.color.accent)
            )
            activeFilters.add(chip.id)
            activeFiltersCount++
            activeFiltersType[chip.text.toString()] = filterType
        }
        else {
            chipView.chipStrokeColor = ColorStateList.valueOf(
                ContextCompat.getColor(requireActivity(), R.color.secondary_text)
            )
            activeFilters.remove(chip.id)
            activeFiltersCount--
            activeFiltersType.remove(chip.text.toString())
        }

        restoreFilters()

    }

    private fun manageSort(chip: CompoundButton, isChecked:Boolean, filterKey:String, filterType:String){
        val chipView = chip as Chip
        if (isChecked) {
            chipView.chipStrokeColor = ColorStateList.valueOf(
                ContextCompat.getColor(requireActivity(), R.color.accent)
            )
            itemAdapter.sortItems(filterType)
            activeFilters.add(chip.id)
            activeFiltersCount++
            activeFiltersType[filterKey] = filterType
        }
        else {
            chipView.chipStrokeColor = ColorStateList.valueOf(
                ContextCompat.getColor(requireActivity(), R.color.secondary_text)
            )
            activeFilters.remove(chip.id)
            activeFiltersCount--
            activeFiltersType.remove(filterKey)
        }

        //TODO check this, fare observer su activeFilterCount e agiornare la vista di conseguenza
        restoreFilters()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putIntegerArrayList("filters",activeFilters)
        outState.putSerializable("filtersType",activeFiltersType)
    }

}