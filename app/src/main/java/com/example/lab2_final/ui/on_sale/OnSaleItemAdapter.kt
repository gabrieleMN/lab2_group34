package com.example.lab2_final.ui.on_sale

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.lab2_final.R
import com.example.lab2_final.objects.Item
import com.example.lab2_final.ui.item.ItemViewModel
import com.squareup.picasso.Picasso

class OnSaleItemAdapter(var items:ArrayList<Item>, var itemViewModel :ItemViewModel): RecyclerView.Adapter<OnSaleItemAdapter.ViewHolder>(), Filterable {

    val itemVM:ItemViewModel = itemViewModel

    var itemFilteredList = ArrayList<Item>()
    var filterType:String? = null

    var searching :Boolean = false
    var restoreList = ArrayList<Item>()

    init{ itemFilteredList = items}

    override fun getItemCount() = itemFilteredList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v= LayoutInflater.from(parent.context)
            .inflate(R.layout.item_layout, parent, false)
        return ViewHolder(v, itemVM)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(itemFilteredList[position])
    }

    override fun onViewRecycled(holder: ViewHolder) {
        holder.unbind()
    }

    class ViewHolder(v: View, itemViewModel: ItemViewModel): RecyclerView.ViewHolder(v) {
        val itemVM = itemViewModel
        val item: ConstraintLayout = v.findViewById(R.id.item)
        val name: TextView = v.findViewById(R.id.item_name)
        val price: TextView = v.findViewById(R.id.item_price)
        val category: TextView = v.findViewById(R.id.item_category)
        val photo: ImageView = v.findViewById(R.id.item_pic)
        val btn_editItem: ImageButton = v.findViewById(R.id.btn_editItem)
        val textViewSold:TextView = v.findViewById(R.id.soldTextView)
        fun bind(i: Item) {
            name.text = i.name
            price.text = i.price.toString()+"€"
            category.text = i.category
            item.setOnClickListener {
                val bundle = Bundle()
                bundle.putBoolean("showInterestedUsers",false)
                itemVM.loadItemInViewModel(i.id)
                Navigation.findNavController(it).navigate(R.id.action_nav_on_sale_items_to_nav_item_details,bundle)
            }
            btn_editItem.visibility = View.GONE
            if(!i.sold)
                textViewSold.visibility = View.GONE
            else
                textViewSold.visibility = View.VISIBLE
            Picasso.get().load(i.imageUrlDownlaod).placeholder(R.drawable.placeholder).into(photo)
        }
        fun unbind(){
            item.setOnClickListener(null)
        }

    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()

                /* Rebase the search starting from the filter already active when search start */
                if(!searching){
                    restoreList.addAll(itemFilteredList)
                    searching = true
                }
                /* If empty search we return to the base */
                if (charSearch.isEmpty()) {
                    itemFilteredList = restoreList
                } else {
                    val resultList = ArrayList<Item>()
                        for (item in restoreList) {
                            if (item.name.toLowerCase().contains(charSearch.toLowerCase())) {
                                resultList.add(item)
                            }
                        }
                    itemFilteredList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = itemFilteredList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if(results != null && results.values != null)
                    itemFilteredList = results.values as ArrayList<Item>
                    notifyDataSetChanged()
            }

        }
    }

    fun performFilter(value:String):Int{
        val resultList = ArrayList<Item>()

        /* Remove the search base, ready for a new one */
        searching = false
        restoreList.clear()

        if(value=="")
            itemFilteredList = items
        else {
            if (filterType == "category")
                for (item in itemFilteredList) {
                    if (item.category == value ) {
                        resultList.add(item)
                    }
                }
            else if (filterType == "price") {
                if (value.contains("More")) {
                    for (item in itemFilteredList) {
                        if (item.price.toDouble() >= value.split(" ")[2].toDouble()) {
                            resultList.add(item)
                        }
                    }
                } else {
                    val fields = value.split(" ")
                    for (item in itemFilteredList) {
                        if (item.price.toDouble() >= fields[0].toDouble() && item.price.toDouble() <= fields[2].toDouble()) {
                            resultList.add(item)
                        }
                    }
                }
            }
            itemFilteredList = resultList
        }
        notifyDataSetChanged()

        return itemFilteredList.size
    }

    fun sortItems(input:String){

        fun selector(item:Item):Double = item.price.toDouble()
        if(input == "asc")
            itemFilteredList.sortBy({selector(it)})
        else
            itemFilteredList.sortByDescending({selector(it)})

        notifyDataSetChanged()

    }

}