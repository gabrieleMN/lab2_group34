package com.example.lab2_final.ui.login

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.lab2_final.R
import androidx.lifecycle.Observer
import com.example.lab2_final.helpers.*
import com.example.lab2_final.ui.profile.ProfileViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.login_fragment.*

class FragmentLogin : Fragment() {
    private val profileModel: ProfileViewModel by activityViewModels()
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = Firebase.auth
    }

    override fun onStart() {
        super.onStart()
        profileModel.getEmailUserLogged().observe(viewLifecycleOwner,
            Observer { emailUsr ->
                if (emailUsr != "") {
                    Log.d("login_vista", "esito login ok " +
                            profileModel.getEmailUserLogged().value!!)
                    btnLogin.isEnabled = true
                    printloginSuccessMessage(requireView(), requireContext())
                    try{
                        findNavController().navigate(R.id.action_fragmentLogin_to_nav_on_sale_items)
                    }
                    catch(e: IllegalArgumentException){
                        e.printStackTrace()
                    }
                }
            }
        )

        profileModel.getFlagLoginFailure().observe(viewLifecycleOwner,
            Observer { loginFailure ->
                if (loginFailure) {
                    Log.d("login_vista", "login failed")
                    btnLogin.isEnabled = true
                    printErrorLoginMessage(requireView(), requireContext())
                }
            })

        val user = auth.currentUser
        if (user != null) {
            profileModel.login(user.email!!)
            findNavController().navigate(R.id.action_fragmentLogin_to_nav_on_sale_items)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // qui inserire i listener per i vari bottoni
        btnLogin.setOnClickListener {
            val mail: String = edit_email.text.toString()
            val pwd: String = edit_password.text.toString()
            var goodInput = true
            if (mail.isEmpty()) {
                Log.d("login_vista", "mail campo vuoto")
                printErrorMessage(requireView(), requireContext())
                goodInput = false
            }
            if (pwd.isEmpty()) {
                Log.d("login_vista", "password campo vuoto")
                printErrorMessage(requireView(), requireContext())
                goodInput = false
            }
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {
                Log.d("login_vista", "mail non valida")
                printEmailNotValid(requireView(), requireContext())
                goodInput = false
            }
            if (goodInput) {
                Log.d("login_vista", "sto tentando il login")
                // perform the login operation
                btnLogin.isEnabled = false

                auth.signInWithEmailAndPassword(mail, pwd)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG_Login", "createUserWithEmail:success")
                            val user = auth.currentUser
                            Log.d("TAG_Login", user?.email!!)
                            profileModel.login(user.email!!)
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("TAG_Login", "createUserWithEmail:failure", task.exception)
                            printFailureSnackbar(requireView(), requireContext(), task.exception!!.message!!)
                            btnLogin.isEnabled = true
                        }
                    }

            }
        }
        btnRegister.setOnClickListener {
            val navig = findNavController()
            navig.navigate(R.id.fragmentRegister)
        }

        val imagebell = requireActivity().findViewById<ImageView>(R.id.bellimage)
        imagebell.visibility=View.GONE

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
        requireActivity().toolbar.navigationIcon = null
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onResume() {
        super.onResume()
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

    }

    override fun onPause() {
        super.onPause()
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR

    }
}