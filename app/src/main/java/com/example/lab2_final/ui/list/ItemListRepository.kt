package com.example.lab2_final.ui.list

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.lab2_final.objects.Item
import com.example.lab2_final.objects.Notification
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.MetadataChanges


class ItemListRepository {

    fun initSnapshotListener(notification:MutableLiveData<Notification>)
            :MutableLiveData<ArrayList<Item>>{
        val items = MutableLiveData<ArrayList<Item>>(ArrayList<Item>())
        val db = Firebase.firestore
        var item: Item?

        db
        .collection("items")
        .whereEqualTo("userOwner", Firebase.auth.currentUser!!.email!!)
        .addSnapshotListener { value, _ ->
            for (dc in value!!.documentChanges) {
                val document = dc.document
                item = document.toObject(Item::class.java)
                item?.setItemId(document.id)
                
                when (dc.type) {
                    DocumentChange.Type.MODIFIED -> {
                        val index = items.value!!.indexOfFirst { it.id == item!!.id }
                        if (index!=-1){
                            items.value!![index] = item!!
                        }
                    }
                    DocumentChange.Type.ADDED -> {
                        items.value!!.add(item!!)
                        activateNotifyForItems(item!!.id, notification)
                    }
                }
            }
            items.value = items.value
        }
        return items
    }


    /* By this we can activate tha notification on every item owned by the user. In this
     * way if someone express his interest to one of our item we will be notified.
     */
    fun activateNotifyForItems(itemId: String, notification: MutableLiveData<Notification>) {
        val db = Firebase.firestore
        db
            .collection("items")
            .document(itemId)
            .collection("emails")
            .addSnapshotListener { value, _ ->
                for (dc in value!!.documentChanges) {
                    if (dc.type == DocumentChange.Type.ADDED) {
                            val userInterested = dc.document.id
                            var n = Notification()
                            n.title = "New interested user!"
                            n.message = userInterested + " is interested in one of your item!"
                            n.read = dc.document.data.get("read") as Boolean
                            n.itemId = itemId
                            n.userInterested = userInterested
                            n.date = dc.document.data.get("date") as String
                            notification.setValue(n)
                        }
                }
            }
    }


    companion object {
        private var INSTANCE: ItemListRepository? = null
        fun getInstance() = INSTANCE
            ?: ItemListRepository().also {
                INSTANCE = it
            }
    }

}