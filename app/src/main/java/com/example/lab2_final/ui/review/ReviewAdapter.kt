package com.example.lab2_final.ui.review

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.lab2_final.R
import com.example.lab2_final.objects.Review
import android.view.View
import android.widget.RatingBar
import android.widget.TextView


class ReviewAdapter(var reviews :ArrayList<Review>) :RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {

    override fun getItemCount() = reviews.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.review_card_layout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ReviewAdapter.ViewHolder, position: Int) {
        holder.bind(reviews[position])
    }

    override fun onViewRecycled(holder: ReviewAdapter.ViewHolder) {
        holder.unbind()
    }

    class ViewHolder(v :View) : RecyclerView.ViewHolder(v) {
        private val reviewerNickname: TextView = v.findViewById(R.id.reviewerNickname)
        private val reviewComment: TextView = v.findViewById(R.id.reviewComment)
        val ratingBar: RatingBar = v.findViewById(R.id.RatingBar)

        fun bind(r :Review) {
            reviewerNickname.text = r.reviewerNickname
            reviewComment.text = r.comment
            ratingBar.rating = r.rate.toFloat()
        }

        fun unbind() {

        }
    }

}