package  com.example.lab2_final.ui.item

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.lab2_final.R
import com.example.lab2_final.helpers.*
import com.example.lab2_final.objects.Item
import com.example.lab2_final.ui.profile.ProfileViewModel
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.default
import id.zelory.compressor.constraint.destination
import id.zelory.compressor.constraint.quality
import kotlinx.android.synthetic.main.fragment_item_edit.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*


const val REQUEST_TAKE_PHOTO = 1
const val REQUEST_LOAD_PHOTO = 2

class ItemEditFragment : Fragment(), OnMapReadyCallback {

    private var addNewItem: Boolean = false
    private var pos: Int = 0
    private var initialized = false
    private val profileViewModel: ProfileViewModel by activityViewModels()
    private val itemViewModel: ItemViewModel by activityViewModels()
    var googleMap: GoogleMap? = null
    var mapView: MapView? = null
    private var mapsSupported = true


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
        val parent= inflater.inflate(R.layout.fragment_item_edit, container, false)
        mapView = parent.findViewById(R.id.mapView7) as MapView
        mapView!!.getMapAsync(this)
        return parent
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(arguments != null) {
            if(requireArguments().getBoolean("Item.addNew")){
                //LIST ITEM REQUEST NEW ITEM
                addNewItem = true
                itemViewModel.deleteImgUrl()
            }else {
                pos = requireArguments().getInt("Item.pos")
            }
        }

        initializeListenerGUI()
        initializeDatePickerGUI()
        initializeCategoriesGUI()

        searchButton.setOnClickListener{
            onSearch()
        }
        itemViewModel.imgDownloadUrl.observe(viewLifecycleOwner, Observer {
            if(!addNewItem) {
                if (!initialized)
                    itemViewModel.getFileFromRepository(profileViewModel.getEmailUserLogged().value!!)
            }
        })


        itemViewModel.photoFile.observe(viewLifecycleOwner, Observer { Picasso.get().load(it).rotate(90f*itemViewModel.numRotation).placeholder(R.drawable.placeholder).into(edit_item_pic)
            initialized = true})
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu_edit, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.save_fragment -> {

                //check if placeholders are empty
                if (TextUtils.isEmpty(edit_item_name.text) ||
                    TextUtils.isEmpty(edit_item_expiryDate.text) ||
                    TextUtils.isEmpty(edit_item_price.text) ||
                    TextUtils.isEmpty(edit_item_description.text) ||
                    TextUtils.isEmpty(edit_item_location.text) ||
                    TextUtils.isEmpty(edit_item_city.text) ||
                    TextUtils.isEmpty(edit_item_category.text) ||
                    TextUtils.isEmpty(edit_item_subcategory.text) ||
                    TextUtils.isEmpty(edit_item_city.text)) {

                    printErrorMessage()
                    return true
                }
/*
                if (currentPhotoPath != previousPhotoPath)
                    deletePreviousPhoto()

 */
                val rotatedBitmap = rotate(itemViewModel.photoFile.value!!.absolutePath, itemViewModel.numRotation)
                itemViewModel.numRotation = 0f
                val photoFile = createImageFile(context as AppCompatActivity)
                photoFile.copyBitmapToFile(rotatedBitmap!!)
                itemViewModel.photoFile.value = photoFile
                val photoURI: Uri = FileProvider.getUriForFile(
                    context as AppCompatActivity,
                    "com.example.lab2_final.fileprovider",
                    itemViewModel.photoFile.value!!
                )
                var item = Item()

                if(!addNewItem){
                    // edit item    
                    item = Item(edit_item_name.text.toString(),
                        edit_item_category.text.toString(),
                        edit_item_subcategory.text.toString(),
                        edit_item_description.text.toString(),
                        edit_item_location.text.toString(),
                        edit_item_city.text.toString(),
                        edit_item_expiryDate.text.toString(),
                        getCurrentDateAndTime(),
                        edit_item_price.text.toString(),
                        profileViewModel.loggedUserEmail.value!!,
                        itemViewModel.isSold.value!!,
                        itemViewModel.imgDownloadUrl.value!!)

                    itemViewModel.updateItem(item, photoURI, profileViewModel.getEmailUserLogged().value!!)

                    itemViewModel.deleteImgUrl()

                    val bundle: Bundle = Bundle()
                    bundle.putBoolean("showInterestedUsers",true)
                    findNavController().navigate(R.id.action_itemEditFragment_to_nav_item_details,bundle)
                } else {
                    // if we are here a new item must be added
                    item = Item(edit_item_name.text.toString(),
                        edit_item_category.text.toString(),
                        edit_item_subcategory.text.toString(),
                        edit_item_description.text.toString(),
                        edit_item_location.text.toString(),
                        edit_item_city.text.toString(),
                        edit_item_expiryDate.text.toString(),
                        getCurrentDateAndTime(),
                        edit_item_price.text.toString(),
                        profileViewModel.loggedUserEmail.value!!)

                    itemViewModel.addItem(item, photoURI, profileViewModel.getEmailUserLogged().value!!)

                    itemViewModel.deleteImgUrl()
                    findNavController().navigate(R.id.action_itemEditFragment_to_nav_item_list)
                }
                printSuccessItemMessage(requireView(),requireContext())
                return true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }


    /*  dispatchTakePictureIntent
    *
    *  This method is invoked to create the intent and dispatch it to the
    *  camera in order to shot the photo.
    *
    */
    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity((context as AppCompatActivity).packageManager)?.also {
                val photoFile =
                    createImageFile(context as AppCompatActivity)
                itemViewModel.photoFile.value = photoFile
                val photoURI: Uri = FileProvider.getUriForFile(
                    context as AppCompatActivity,
                    "com.example.lab2_final.fileprovider",
                    itemViewModel.photoFile.value!!
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)

            }
        }
    }
    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        activity?.menuInflater?.inflate(R.menu.layout_context_menu, menu)
        //Controls if the device has a Camera, if not disables the take photos option from the menu
        if (! (context as AppCompatActivity).baseContext.packageManager.hasSystemFeature(
                PackageManager.FEATURE_CAMERA_ANY) ) {
            menu.findItem(R.id.camera)?.isEnabled = false
        }

    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.gallery -> {
                val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(pickPhoto,
                    REQUEST_LOAD_PHOTO
                )
                return true
            }
            R.id.camera -> { dispatchTakePictureIntent(); return true }
            R.id.cancel -> { return true }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {

        } else if (requestCode == REQUEST_LOAD_PHOTO && resultCode == Activity.RESULT_OK){
            /* save the photo in our internal storage (FilesDir), in this way if the
               user delete it from gallery it will available for the application anyway
            */

            val destinationFile = createImageFile(context as AppCompatActivity)
            val picToSave = data?.data as Uri
            val inputStream = (context as AppCompatActivity).contentResolver.openInputStream(picToSave) as InputStream

            destinationFile.copyInputStreamToFile(inputStream)
            itemViewModel.photoFile.value = destinationFile

        } else if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_CANCELED){
            return
        } else if (requestCode == REQUEST_LOAD_PHOTO && resultCode == Activity.RESULT_CANCELED){
            return
        }

        GlobalScope.launch {
            compressAndLoad()
        }
        Picasso.get().load(itemViewModel.photoFile.value!!).placeholder(R.drawable.placeholder).into(edit_item_pic)

    }


    private suspend fun compressAndLoad() {
        try {
            Compressor.compress(context as AppCompatActivity, itemViewModel.photoFile.value!!) {
                quality(30)
                default()
                destination(itemViewModel.photoFile.value!!)
            }
        } catch (e: IOException) {
            Log.d("compressAndLoad", "Compress and load in ItemEditfragment Failed.")
        }
    }

    private fun limitRange(): CalendarConstraints.Builder? {
        val constraintsBuilder = CalendarConstraints.Builder()
        val calendarStart: Calendar = Calendar.getInstance()
        val calendar: Calendar = GregorianCalendar()
        calendarStart.set(calendar.get(Calendar.YEAR) ,calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH) )

        val minDate: Long = calendarStart.timeInMillis
        constraintsBuilder.setStart(minDate)
        constraintsBuilder.setValidator(
            Validator(
                minDate
            )
        )
        return constraintsBuilder
    }

    internal class Validator : CalendarConstraints.DateValidator {
        var minDate: Long

        constructor(minDate: Long) {
            this.minDate = minDate
        }

        constructor(parcel: Parcel) {
            minDate = parcel.readLong()
        }

        override fun isValid(date: Long): Boolean {
            return minDate <= date
        }
        override fun describeContents(): Int {
            return 0
        }
        override fun writeToParcel(dest: Parcel, flags: Int) {
            dest.writeLong(minDate)
        }
        companion object {
            val CREATOR: Parcelable.Creator<Validator?> = object : Parcelable.Creator<Validator?> {
                override fun createFromParcel(parcel: Parcel): Validator {
                    return Validator(
                        parcel
                    )
                }

                override fun newArray(size: Int): Array<Validator?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }

    //CALLED AFTER ONVIEWCREATED !!
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        super.onCreate(savedInstanceState)
        try {
            MapsInitializer.initialize(getActivity())
        } catch (e: GooglePlayServicesNotAvailableException) {
            mapsSupported = false
        }
        if (mapView != null) {
            mapView!!.onCreate(savedInstanceState)
        }
        initializeMap()

        if(savedInstanceState!=null){
            addNewItem = savedInstanceState.getBoolean("addNewItem")
            pos = savedInstanceState.getInt("position")
            initialized = savedInstanceState.getBoolean("initialized")
        }

        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
    }

    private fun initializeListenerGUI(){

        btn_rotate.setOnClickListener {
           itemViewModel.numRotation += 1f
            Picasso.get().load(itemViewModel.photoFile.value!!).placeholder(R.drawable.placeholder).rotate(90f*itemViewModel.numRotation).into(edit_item_pic)
        }

        registerForContextMenu(camera_button)
        camera_button.setOnClickListener { camera_button.showContextMenu() }

    }

    private fun initializeDatePickerGUI(){

        val builder = MaterialDatePicker.Builder.datePicker()
        builder.setTitleText("Select a Date")
        builder.setCalendarConstraints(limitRange()?.build())
        val picker=builder.build()

        edit_item_expiryDate.setOnClickListener { picker.show(requireFragmentManager(), picker.toString()) }

        picker.addOnPositiveButtonClickListener {
            val data:Long="$it".toLong()
            val itemDateStr: String = SimpleDateFormat("yyyy-MM-dd").format(data)
            edit_item_expiryDate.setText(itemDateStr)
        }
    }

    private fun initializeCategoriesGUI(){
        val adapter: ArrayAdapter<String?> = ArrayAdapter(requireContext(),
            R.layout.dropdownitem,
            resources.getStringArray(R.array.categories)
        )
        edit_item_category.setAdapter(adapter)

        edit_item_category.onItemClickListener=
            AdapterView.OnItemClickListener { p0, p1, p2, p3 ->
                val elementocateg = p0?.getItemAtPosition(p2).toString()
                edit_item_subcategory.setText("")
                when (elementocateg) {
                    "Art & Crafts" -> {
                        edit_item_subcategory.hint = resources.getStringArray(
                            R.array.ArtCrafts)[0]
                        edit_item_subcategory.setAdapter(ArrayAdapter(requireContext(),
                        R.layout.dropdownitem, resources.getStringArray(R.array.ArtCrafts)))}
                    "Sports & Hobby" -> {
                        edit_item_subcategory.hint = resources.getStringArray(
                            R.array.SportsHobby)[0]
                        edit_item_subcategory.setAdapter(ArrayAdapter(requireContext(),
                        R.layout.dropdownitem, resources.getStringArray(R.array.SportsHobby)))}
                    "Baby" -> {
                        edit_item_subcategory.hint = resources.getStringArray(R.array.Baby)[0]
                        edit_item_subcategory.setAdapter(ArrayAdapter(requireContext(),
                        R.layout.dropdownitem, resources.getStringArray(R.array.Baby)))}
                    "Women's fashion" -> {
                        edit_item_subcategory.hint = resources.getStringArray(R.array.women)[0]
                        edit_item_subcategory.setAdapter(ArrayAdapter(requireContext(),
                        R.layout.dropdownitem, resources.getStringArray(R.array.women)))}
                    "Men's fashion" -> {
                        edit_item_subcategory.hint = resources.getStringArray(R.array.men)[0]
                        edit_item_subcategory.setAdapter(ArrayAdapter(requireContext(),
                        R.layout.dropdownitem, resources.getStringArray(R.array.men)))}
                    "Electronics" -> {
                        edit_item_subcategory.hint = resources.getStringArray(R.array.electronics)[0]
                        edit_item_subcategory.setAdapter(ArrayAdapter(requireContext(),
                        R.layout.dropdownitem, resources.getStringArray(R.array.electronics)))}
                    "Games & Videogames" -> {
                        edit_item_subcategory.hint = resources.getStringArray(R.array.videogames)[0]
                        edit_item_subcategory.setAdapter(ArrayAdapter(requireContext(),
                        R.layout.dropdownitem, resources.getStringArray(R.array.electronics)))}
                    "Automotive" -> {
                        edit_item_subcategory.hint = resources.getStringArray(R.array.automotive)[0]
                        edit_item_subcategory.setAdapter(ArrayAdapter(requireContext(),
                        R.layout.dropdownitem, resources.getStringArray(R.array.automotive)))}
                }
            }


        if(edit_item_category.text.toString() == "Art & Crafts"){
            val subcatAdapter=ArrayAdapter(requireContext(),R.layout.dropdownitem,resources.getStringArray(R.array.ArtCrafts))
            edit_item_subcategory.setAdapter(subcatAdapter)
        }else if(edit_item_category.text.toString() == "Sports & Hobby"){
            val subcatAdapter=ArrayAdapter(requireContext(),R.layout.dropdownitem,resources.getStringArray(R.array.SportsHobby))
            edit_item_subcategory.setAdapter(subcatAdapter)
        }else if(edit_item_category.text.toString() == "Baby"){
            val subcatAdapter=ArrayAdapter(requireContext(),R.layout.dropdownitem,resources.getStringArray(R.array.Baby))
            edit_item_subcategory.setAdapter(subcatAdapter)
        }else if(edit_item_category.text.toString() == "Women's fashion"){
            val subcatAdapter=ArrayAdapter(requireContext(),R.layout.dropdownitem,resources.getStringArray(R.array.women))
            edit_item_subcategory.setAdapter(subcatAdapter)
        }else if(edit_item_category.text.toString() == "Men's fashion"){
            val subcatAdapter=ArrayAdapter(requireContext(),R.layout.dropdownitem,resources.getStringArray(R.array.men))
            edit_item_subcategory.setAdapter(subcatAdapter)
        }else if(edit_item_category.text.toString() == "Electronics"){
            val subcatAdapter=ArrayAdapter(requireContext(),R.layout.dropdownitem,resources.getStringArray(R.array.electronics))
            edit_item_subcategory.setAdapter(subcatAdapter)
        }else if(edit_item_category.text.toString() == "Games & Videogames"){
            val subcatAdapter=ArrayAdapter(requireContext(),R.layout.dropdownitem,resources.getStringArray(R.array.videogames))
            edit_item_subcategory.setAdapter(subcatAdapter)
        }else if(edit_item_category.text.toString() == "Automotive"){
            val subcatAdapter=ArrayAdapter(requireContext(),R.layout.dropdownitem,resources.getStringArray(R.array.automotive))
            edit_item_subcategory.setAdapter(subcatAdapter)
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if(this.isVisible) {
            outState.putBoolean("addNewItem", addNewItem)
            outState.putInt("position", pos)
            outState.putBoolean("initialized",initialized)
        }
    }

    private fun printErrorMessage(){
        val snackbar =
            Snackbar.make(requireView(), "Fields cannot be empty", Snackbar.LENGTH_LONG)
                .setAction("Action", null)
        val sbView = snackbar.view
        sbView.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.design_default_color_error));
        snackbar.show()
    }

    override fun onStart() {
        super.onStart()
        if(!addNewItem)
            registerObserversItemDetails(itemViewModel)
    }

    fun registerObserversItemDetails(detailsVM: ItemViewModel){
        if (!initialized) {
            detailsVM.nameVM.observe(viewLifecycleOwner, Observer { item_name ->
                this.edit_item_name.setText(item_name)
            })
            detailsVM.cateogoryVM.observe(
                viewLifecycleOwner,
                Observer { category -> this.edit_item_category.setText(category) })
            detailsVM.subcategoryVM.observe(
                viewLifecycleOwner,
                Observer { subcategory -> this.edit_item_subcategory.setText(subcategory) })
            detailsVM.priceVM.observe(
                viewLifecycleOwner,
                Observer { price -> this.edit_item_price.setText(price) })
            detailsVM.descriptionVM.observe(
                viewLifecycleOwner,
                Observer { description -> this.edit_item_description.setText(description) })
            detailsVM.locationVM.observe(
                viewLifecycleOwner,
                Observer { location -> this.edit_item_location.setText(location) })
            detailsVM.isSold.observe(
                viewLifecycleOwner,
                Observer {})
            detailsVM.cityVM.observe(
                viewLifecycleOwner,
                Observer { city -> this.edit_item_city.setText(city) })
            detailsVM.expiryDateVM.observe(
                viewLifecycleOwner,
                Observer { date -> this.edit_item_expiryDate.setText(date) })

        }
    }

    override fun onResume() {
        super.onResume()
        mapView!!.onResume()
        initializeMap()
    }
 
    override fun onMapReady(p0: GoogleMap?) {
        googleMap=p0

        var address: Address? = null
        var addressList: List<Address>? = null
        googleMap?.setOnMapClickListener(GoogleMap.OnMapClickListener { point ->
            googleMap?.clear()
            googleMap?.addMarker(MarkerOptions().position(point))
            googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom((point),10.0F))

            val geocoder = Geocoder(requireActivity())
            try {
                addressList = geocoder.getFromLocation(point.latitude, point.longitude, 1)
                address = addressList!![0]
                edit_item_location.setText((address!!.getAddressLine(0)) +" " +address!!.locality + " " + " " + address!!.adminArea)

            } catch (e: Exception) {
                e.printStackTrace()

            }

        })
        val location = edit_item_location.text.toString()
        if (location != null || location != "") {
            val geocoder = Geocoder(requireActivity())
            try {
                addressList = geocoder.getFromLocationName(location, 1)
                address = addressList!![0]


            } catch (e: Exception) {
                e.printStackTrace()
                return;
            }
            googleMap!!.clear()
            val latLng = LatLng(address!!.getLatitude(), address!!.getLongitude())
            googleMap?.addMarker(MarkerOptions().position(latLng))
            googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom((latLng),15.0F))


        }
    }
    private fun initializeMap() {
        if (googleMap == null && mapsSupported) {
            mapView = getActivity()?.findViewById(R.id.mapView7)
            //setup markers etc...
        }
    }

    fun onSearch() {
        var address: Address? = null
        val location = edit_item_location.text.toString()
        var addressList: List<Address>? = null
        if (location != null || location != "") {
            val geocoder = Geocoder(requireActivity())
            try {
                addressList = geocoder.getFromLocationName(location, 1)
                address = addressList!![0]
                googleMap!!.clear()
                val latLng = LatLng(address!!.getLatitude(), address!!.getLongitude())
                googleMap?.addMarker(MarkerOptions().position(latLng))
                googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom((latLng),15.0F))

            } catch (e: Exception) {
                MaterialAlertDialogBuilder(context)
                    .setTitle("Location")
                    .setMessage("Try again to insert the right address")
                    .setPositiveButton("ok") { dialog, which ->
                        return@setPositiveButton;
                    }
                    .show()
            }
        }
    }

}
