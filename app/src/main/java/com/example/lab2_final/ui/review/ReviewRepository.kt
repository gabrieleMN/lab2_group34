package com.example.lab2_final.ui.review

import androidx.lifecycle.MutableLiveData
import com.example.lab2_final.helpers.addToList
import com.example.lab2_final.objects.Review
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase



class ReviewRepository {


    fun getReviews(currentUserEmail :String) :MutableLiveData<ArrayList<Review>> {
        val reviews = MutableLiveData<ArrayList<Review>>(ArrayList())
        val db = Firebase.firestore

        db
        .collection("utenti")
        .document(currentUserEmail)
        .collection("reviews")
            .get()
            .addOnSuccessListener { res ->
                if (res != null)
                    for (dc in res.documentChanges)
                        reviews.addToList(dc.document.toObject(Review::class.java))
            }

        return reviews
    }

    companion object {
        private var INSTANCE: ReviewRepository? = null
        fun getInstance() = INSTANCE
            ?: ReviewRepository().also {
                INSTANCE = it
            }
    }
}