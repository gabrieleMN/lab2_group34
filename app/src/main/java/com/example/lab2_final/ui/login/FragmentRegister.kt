package com.example.lab2_final.ui.login

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.lab2_final.R
import com.example.lab2_final.helpers.printEmailNotValid
import com.example.lab2_final.helpers.printErrorMessage
import com.example.lab2_final.helpers.printErrorRegisterMessage
import com.example.lab2_final.helpers.printFailureSnackbar
import com.example.lab2_final.ui.profile.ProfileViewModel
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.login_fragment.*
import kotlinx.android.synthetic.main.register_fragment.*
import kotlinx.android.synthetic.main.register_fragment.btnRegister
import kotlinx.android.synthetic.main.register_fragment.edit_email
import kotlinx.android.synthetic.main.register_fragment.edit_password

class FragmentRegister : Fragment() {
    private val profileModel: ProfileViewModel by activityViewModels()
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = Firebase.auth
    }

    override fun onStart() {
        super.onStart()
        // qui inserire un observer che permetterà di navigare nella item_list se
        // la registration va a buon fine e quindi se emailUsr viene popolata
        profileModel.getEmailUserLogged().observe(viewLifecycleOwner,
        Observer { emailUsr ->
            if (emailUsr != "") {
                Log.d("registration_vista", "registration done " +
                        profileModel.getEmailUserLogged().value!!)
                profileModel.loadUserInViewModel(emailUsr)
                requireActivity().findViewById<NavigationView>(R.id.nav_view).setCheckedItem(R.id.nav_show_profile)
                findNavController().navigate(R.id.action_fragmentRegister_to_nav_edit_profile)
            }
        })

        profileModel.getFlagUserExist().observe(viewLifecycleOwner,
            Observer { userExist ->
                if (userExist) {
                    Log.d("registration_vista", "user already exist")
                    printErrorRegisterMessage(requireView(), requireContext())

                }
            }
        )
        /*
        profileModel.userEmailVM.observe(viewLifecycleOwner,
            Observer {
                findNavController().navigate(
                    R.id.action_fragmentRegister_to_nav_edit_profile
                )
            }
        )
         */
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.register_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //qui inserire i listener per i vari bottoni
        btnRegister.setOnClickListener {
            val mail: String = edit_email.text.toString()
            val pwd: String = edit_password.text.toString()
            var goodInput = true
            if (mail.isEmpty()) {
                Log.d("registration_vista", "mail campo vuoto")
                printErrorMessage(requireView(), requireContext())
                goodInput = false
            }
            if (pwd.isEmpty()) {
                Log.d("registration_vista", "password campo vuoto")
                printErrorMessage(requireView(), requireContext())
                goodInput = false
            }
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {
                Log.d("login_vista", "mail non valida")
                printEmailNotValid(requireView(), requireContext())
                goodInput = false
            }
            if (goodInput) {
                auth.createUserWithEmailAndPassword(mail, pwd)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG_Registrazione", "createUserWithEmail:success")
                            val user = auth.currentUser
                            Log.d("TAG_Registrazione", user?.email!!)
                            profileModel.register(user.email!!, pwd)
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("TAG_Registrazione", "createUserWithEmail:failure", task.exception)
                            printFailureSnackbar(requireView(), requireContext(), task.exception!!.message!!)
                            btnRegister.isEnabled = true
                        }
                    }
                Log.d("registration_vista", "sto tentando la registration")


            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onResume() {
        super.onResume()
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

    }

    override fun onPause() {
        super.onPause()
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
    }
}