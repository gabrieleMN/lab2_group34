package com.example.lab2_final.ui.notification

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.lab2_final.R
import com.example.lab2_final.objects.Notification
import com.example.lab2_final.ui.item.ItemViewModel

class NotificationListAdapter(var notifications: ArrayList<Notification>,var itemVM:ItemViewModel,var notificationListVM:NotificationListViewModel,var userLogged:String)
    : RecyclerView.Adapter<NotificationListAdapter.ViewHolder>() {


    override fun getItemCount() = notifications.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v= LayoutInflater.from(parent.context)
            .inflate(R.layout.notification_layout, parent, false)
        return ViewHolder(v,itemVM,notificationListVM,userLogged)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(notifications[position])
    }

    override fun onViewRecycled(holder: ViewHolder) {
        holder.unbind()
    }


    class ViewHolder(v: View,
                     var itemVM:ItemViewModel,
                     var notificationListVM:NotificationListViewModel ,
                     var userLogged:String): RecyclerView.ViewHolder(v) {

        val message: TextView = v.findViewById(R.id.message)
        val date:TextView = v.findViewById(R.id.date)
        val title:TextView = v.findViewById(R.id.title)
        val cardView: CardView = v.findViewById(R.id.notificationCardView)
        fun bind(n: Notification) {
            message.text = n.message
            date.text = n.date
            title.text = n.title
            cardView.setOnClickListener{
                itemVM.loadItemInViewModel(n.itemId)
                val bundle = Bundle()
                bundle.putBoolean("showInterestedUsers",true)
                Navigation.findNavController(it).navigate(R.id.action_notificationListFragment_to_nav_item_details,bundle)
                if(n.userInterested != "") {
                    notificationListVM.readNotificationItemInterest(n)
                }
                else{
                    notificationListVM.readNotificationItemSold(userLogged, n)
                }
            }
        }

        fun unbind() {
            cardView.setOnClickListener(null)
        }
    }
}