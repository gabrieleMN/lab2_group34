package com.example.lab2_final.ui.sold_item_interested_users

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import com.example.lab2_final.R
import com.example.lab2_final.objects.User
import com.example.lab2_final.ui.item.ItemViewModel
import kotlinx.android.synthetic.main.fragment_sold_item_interested_users.*
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager


class SoldItemInterestedUsers : Fragment() {

    private lateinit var interestedUserAdapter: InterestedUserAdapter
    private val itemViewModel : ItemViewModel by activityViewModels()


    private var userList:ArrayList<User> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        interestedUserAdapter = InterestedUserAdapter(userList, itemViewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sold_item_interested_users, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView_buyers.adapter = interestedUserAdapter

        //set the layout manager, should only be set once
        recyclerView_buyers.layoutManager = LinearLayoutManager(context)
    }

    override fun onStart() {
        super.onStart()
        registerObserversRealTimeSoldItemInterestedUsers(itemViewModel)
    }


    fun registerObserversRealTimeSoldItemInterestedUsers(detailsVM: ItemViewModel)
    {
        /* Manage real time changes on interested user list */
        detailsVM.userInterestedVM.observe(viewLifecycleOwner, Observer { listUser ->
            interestedUserAdapter.items = listUser
            interestedUserAdapter.notifyDataSetChanged()
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
    }
}
