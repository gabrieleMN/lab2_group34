package com.example.lab2_final.ui.item

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.lab2_final.helpers.DEFAULT_IMAGE_URI
import com.example.lab2_final.objects.Item
import com.example.lab2_final.objects.User
import com.example.lab2_final.ui.profile.ProfileRepository
import java.io.File

class ItemViewModel : ViewModel() {

    private var itemId: MutableLiveData<String> = MutableLiveData("")

    private val itemVMRealTime: LiveData<Item> = Transformations.switchMap(itemId) { id ->
        if(id != "")
            ItemRepository.getInstance().realTimeUpdate(id)
        else
            MutableLiveData<Item>(Item())
    }

    val nameVM :LiveData<String> = Transformations.map(itemVMRealTime){ item -> item.name }
    val cateogoryVM :LiveData<String> = Transformations.map(itemVMRealTime){  item -> item.category }
    val subcategoryVM :LiveData<String> = Transformations.map(itemVMRealTime){  item -> item.subCategory }
    val priceVM :LiveData<String> = Transformations.map(itemVMRealTime){  item -> item.price }
    val descriptionVM :LiveData<String> = Transformations.map(itemVMRealTime){  item -> item.description }
    val locationVM :LiveData<String> = Transformations.map(itemVMRealTime){  item -> item.location }
    val cityVM :LiveData<String> = Transformations.map(itemVMRealTime){  item -> item.city }
    val expiryDateVM :LiveData<String> = Transformations.map(itemVMRealTime){  item -> item.expiryDate }
    val creationDateVM :LiveData<String> = Transformations.map(itemVMRealTime){  item -> item.creationDate }
    val emailsInterestedVM :LiveData<ArrayList<String>> = Transformations.map(itemVMRealTime){ item->item.emailInterested }
    val userInterestedVM :LiveData<ArrayList<User>> =Transformations.switchMap(emailsInterestedVM){ list->
        ProfileRepository.getInstance().getUserList(list)
    }
    val buyerEmail :LiveData<String> = Transformations.map(itemVMRealTime){ item -> item.buyer }
    val reviewed :LiveData<Boolean> = Transformations.map(itemVMRealTime){ item -> item.reviewed }
    val itemOwner:LiveData<String> = Transformations.map(itemVMRealTime){ item -> item.userOwner }
    val itemOwnerImg: LiveData<String> = Transformations.switchMap(itemOwner){ owner ->
        if(owner != "")
            ItemRepository.getInstance().getOwnerImg(owner)
        else
            MutableLiveData("")
    }
    val imgDownloadUrl :LiveData<String> = Transformations.map(itemVMRealTime){ item -> item.imageUrlDownlaod }
    var photoFile :MutableLiveData<File> = ItemRepository.getInstance().initImageIntoFile()
    var numRotation = 0f


    val isSold: LiveData<Boolean> = Transformations.map(itemVMRealTime){ item -> item.sold }

    fun updateItem(item:Item, imageUri: Uri, userLoggedEmail: String) {
        ItemRepository.getInstance().updateItem(item, itemId, imageUri, userLoggedEmail)
    }

    fun loadItemInViewModel(itemId:String){
        this.itemId.value = itemId
    }

    fun addItem(item :Item, imageUri: Uri, userLoggedEmail: String){
        ItemRepository.getInstance().addItem(item, imageUri, userLoggedEmail)
    }

    fun markItemAsSold(buyerEmail: String){
        ItemRepository.getInstance().markItemAsSold(itemId, buyerEmail)
    }

    fun addPreferenceToItem(emailUserLogged: String) {
        ItemRepository.getInstance().addPreferenceToItem(itemId.value!!, emailUserLogged)
    }

    fun removePreferenceToItem(emailUserLogged: String){
        ItemRepository.getInstance().removePreferenceToItem(itemId.value!!, emailUserLogged)
    }

    fun getFileFromRepository(emailUserLogged: String){
        ItemRepository.getInstance().loadImageIntoFile(itemId.value!!,emailUserLogged,photoFile)
    }

    fun deleteImgUrl() {
        itemVMRealTime.value?.imageUrlDownlaod = DEFAULT_IMAGE_URI
        //todo da cambiare! Metti l'immagina di default come variabile istanza e non riprenderla ogni volta!!!!
        photoFile = ItemRepository.getInstance().initImageIntoFile()
        itemId.value = itemId.value
    }

    fun reviewed() {
        ItemRepository.getInstance().reviewed(itemId.value!!)
    }
}