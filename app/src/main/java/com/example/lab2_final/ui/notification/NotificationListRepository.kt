package com.example.lab2_final.ui.notification

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.lab2_final.objects.Notification
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class NotificationListRepository {

    fun readNotificationItemInterest(itemId:String, notificationId:String,onResult: (alreadySold: Boolean) -> Unit){
        val db = Firebase.firestore
        db
            .collection("items")
            .document(itemId)
            .collection("emails")
            .document(notificationId)
            .update("read",true)
            .addOnSuccessListener { onResult(false) }
            .addOnFailureListener{onResult(true)}

        /*  If the query fails it means that we have already deleted notifications field in the database
         *  because we have sold the item before reading the notification. Selling an item automaticcaly
         *  delete all the user interested in it.
         */
    }

    fun readNotificationItemSold(userId: String, notificationId: String,onResult: (alreadySold: Boolean) -> Unit) {
        val db = Firebase.firestore
        db
            .collection("utenti")
            .document(userId)
            .collection("items")
            .document(notificationId)
            .update("read",true)
            .addOnSuccessListener { onResult(false) }
            .addOnFailureListener{onResult(true)} //TODO useless, to delete
    }

    fun activateNotifications(notifications:MutableLiveData<ArrayList<Notification>>){
        val db = Firebase.firestore
        db
            .collection("utenti")
            .document(Firebase.auth.currentUser!!.email!!)
            .collection("items")
            .whereEqualTo("sold",true)
            .whereEqualTo("read",false)
            .addSnapshotListener { value, _ ->
                for (dc in value!!.documentChanges) {
                    /* The flag read is not needed because when an notification is read it is
                    *  automatically deleted from the db.
                    *
                    * Type.ADDED    in order to retrieve old notification but still not read by the
                    *               user. This are actual notification only if the flag sold=true.
                    *
                    * Type.MODIFIED in order to be notified when an item is sold.
                    */
                    if(dc.type == DocumentChange.Type.ADDED || dc.type == DocumentChange.Type.MODIFIED) {
                            val n = Notification()
                            val imBuyer = dc.document.data["imBuyer"] as Boolean
                            if(imBuyer) {
                                n.title = "New purchase!"
                                n.message = "Congratulations for your purchase!"
                            }
                            else {
                                n.title = " Item sold!"
                                n.message = "An item you've been interested has been sold!"
                            }
                            n.itemId = dc.document.id
                            n.date = dc.document.data["date"] as String
                            n.userInterested =""
                            n.read = false
                            notifications.value!!.add(0,n)
                    }
                }
                notifications.value = notifications.value
            }
    }

    companion object {
        private var INSTANCE: NotificationListRepository? = null
        fun getInstance() = INSTANCE
            ?: NotificationListRepository().also {
                INSTANCE = it
            }
    }
}