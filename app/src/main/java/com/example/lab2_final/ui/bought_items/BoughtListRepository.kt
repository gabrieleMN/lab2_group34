package com.example.lab2_final.ui.bought_items

import androidx.lifecycle.MutableLiveData
import com.example.lab2_final.helpers.addToList
import com.example.lab2_final.objects.Item
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class BoughtListRepository {

    fun getBoughtItems():MutableLiveData<ArrayList<Item>>{
        val boughtItems = MutableLiveData<ArrayList<Item>>(ArrayList())
        val db = Firebase.firestore

        db
            .collection("utenti")
            .document( Firebase.auth.currentUser!!.email!!)
            .collection("items")
            .whereEqualTo("sold",true)
            .whereEqualTo("imBuyer",true)
            .addSnapshotListener{value,_ ->
                for (dc in value!!.documentChanges) {
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> {
                           getItemFromId(boughtItems,dc.document.id)
                        }
                    }
                }
            }
        return boughtItems
    }

    private fun getItemFromId(boughtItems:MutableLiveData<ArrayList<Item>>,itemId:String){
        /*  Here we don't need a snapshot listener. When the object is sold, apart of the
         *  review flag in which we are not interested in, it can't change. For this reason
         *  we don't need real time changes to be managed.
         */
        val db = Firebase.firestore
        db
            .collection("items")
            .document(itemId)
            .get()
            .addOnSuccessListener {
                var item = it.toObject(Item::class.java)
                item!!.setItemId(itemId)
                boughtItems.addToList(item!!)
            }
    }

    companion object {
        private var INSTANCE: BoughtListRepository? = null
        fun getInstance() = INSTANCE
            ?: BoughtListRepository().also {
                INSTANCE = it
            }
    }

}