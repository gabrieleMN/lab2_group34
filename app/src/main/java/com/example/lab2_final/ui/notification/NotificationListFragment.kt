package com.example.lab2_final.ui.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lab2_final.R
import com.example.lab2_final.ui.item.ItemViewModel
import com.example.lab2_final.ui.profile.ProfileViewModel
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.notifications_list_fragment.*

class NotificationListFragment : Fragment(){

    private lateinit var notificationAdapter: NotificationListAdapter
    private lateinit var notificationViewModel: NotificationListViewModel
    private lateinit var itemVM: ItemViewModel

    override fun onStart() {
        super.onStart()

        notificationViewModel.getNotificationsObservable().observe(viewLifecycleOwner, Observer { notifications ->
            notificationAdapter.notifications = notifications
            notificationAdapter.notifyDataSetChanged()

            empty_list_action.setOnClickListener {
                Navigation.findNavController(it).navigate(R.id.action_notificationListFragment_to_nav_on_sale_items)
            }

            if(notificationViewModel.getNotificationsObservable().value!!.size == 0) {
                empty_list.visibility = View.VISIBLE
                empty_list_action.visibility = View.VISIBLE
            }else{
                empty_list.visibility = View.GONE
                empty_list_action.visibility = View.GONE
            }

        })

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notificationViewModel = ViewModelProvider(requireActivity()).get(NotificationListViewModel::class.java)
        itemVM =  ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        val profileVM =  ViewModelProvider(requireActivity()).get(ProfileViewModel::class.java)
        notificationAdapter = NotificationListAdapter(notificationViewModel.getNotificationsObservable().value!!,itemVM,notificationViewModel,profileVM.getEmailUserLogged().value!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
        return inflater.inflate(R.layout.notifications_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //set the adapter and layoutManager to the recycler view
        notificationsRecyclerView.adapter = notificationAdapter

        //set the layout manager, should only be set once
        notificationsRecyclerView.layoutManager = LinearLayoutManager(context)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
        //TODO CHANGE IN nav_notification
        requireActivity().findViewById<NavigationView>(R.id.nav_view).setCheckedItem(R.id.notificationListFragment)
    }
}