package com.example.lab2_final.ui.profile

import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.lab2_final.R
import com.example.lab2_final.helpers.*
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.navigation.NavigationView
import com.squareup.picasso.Picasso
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.default
import id.zelory.compressor.constraint.destination
import id.zelory.compressor.constraint.quality
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import kotlinx.android.synthetic.main.fragment_edit_profile.num_sold_textview
import kotlinx.android.synthetic.main.fragment_edit_profile.rating_textview
import kotlinx.android.synthetic.main.fragment_show_profile.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import java.io.InputStream


const val REQUEST_TAKE_PHOTO = 1
const val REQUEST_LOAD_PHOTO = 2

class EditProfileFragment : Fragment(), OnMapReadyCallback {
    private val profileModel: ProfileViewModel by activityViewModels()
    private var initialized = false


    var googleMap: GoogleMap? = null
    var mapView: MapView? = null
    private var mapsSupported = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val parent =
            inflater.inflate(R.layout.fragment_edit_profile, container, false)
        mapView = parent.findViewById(R.id.mapView7) as MapView
        mapView!!.getMapAsync(this)
        return parent
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""

        initializeListenerGUI()

        profileModel.photoFile.observe(viewLifecycleOwner, Observer {
            Picasso.get().load(it).placeholder(R.drawable.profile_demo)
                .rotate(90f*profileModel.numRotation).into(edit_profilePic)
            initialized = true})

        profileModel.userNReviews.observe(viewLifecycleOwner, Observer { nReviews ->
            if (profileModel.userRateReviewTot.value != null && profileModel.userRateReviewTot.value!! != 0)
                rating_textview.text = (profileModel.userRateReviewTot.value!!.toFloat() / nReviews.toFloat()).toString()
        })

        searchButton.setOnClickListener{ onSearch() }

        val imagebell = requireActivity().findViewById<ImageView>(R.id.bellimage)
        imagebell.visibility = View.GONE

    }

    override fun onStart() {
        super.onStart()
        if (!initialized){

            profileModel.userNameVM.observe(viewLifecycleOwner,
                Observer { userName ->
                    edit_fullname.setText(userName)
                })
            profileModel.userNicknameVM.observe(viewLifecycleOwner,
                Observer { userNickname ->
                    edit_nickname.setText(userNickname)
                })
            profileModel.userEmailVM.observe(viewLifecycleOwner,
                Observer { userEmail ->
                    edit_email.setText(userEmail)
                })
            profileModel.userGeoAreaVM.observe(viewLifecycleOwner,
                Observer { userGeoArea ->
                    edit_geoArea.setText(userGeoArea)
                })
            profileModel.userTelephoneVM.observe(viewLifecycleOwner,
                Observer { userTelephone ->
                    edit_telephone.setText(userTelephone)
                })
            profileModel.userPasswordVM.observe(viewLifecycleOwner,
                Observer { userPassword ->
                    edit_password.setText(userPassword)
                })

            profileModel.imgDownloadUrl.observe(viewLifecycleOwner, Observer { profileModel.getFileFromRepository() })
        }

        profileModel.userSoldCounter.observe(viewLifecycleOwner,
            Observer { soldCounter ->
                num_sold_textview.text = soldCounter.toString()
            })

    }


    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        activity?.menuInflater?.inflate(R.menu.layout_context_menu, menu)
        //Controls if the device has a Camera, if not disables the take photos option from the menu
        if (! (context as AppCompatActivity).baseContext.packageManager
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY) ) {
            menu.findItem(R.id.camera)?.isEnabled = false
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.layout_options_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.gallery -> {
                val pickPhoto = Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(pickPhoto, REQUEST_LOAD_PHOTO)
                return true
            }
            R.id.camera -> { dispatchTakePictureIntent(); return true }
            R.id.cancel -> { return true }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.save -> {

                if( TextUtils.isEmpty(edit_fullname.text.toString())||
                    TextUtils.isEmpty(edit_nickname.text.toString())||
                    TextUtils.isEmpty(edit_email.text.toString())||
                    TextUtils.isEmpty(edit_geoArea.text.toString())||
                    TextUtils.isEmpty(edit_telephone.text.toString())||
                    TextUtils.isEmpty((edit_password.text.toString()))){

                    printErrorMessage(requireView(), requireContext())
                    return true
                }

                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edit_email.text.toString()).matches()) {
                    printEmailNotValid(requireView(), requireContext())
                    return true
                }

                val rotatedBitmap = rotate(profileModel.photoFile.value!!.absolutePath, profileModel.numRotation)
                profileModel.numRotation = 0f
                val photoFile = createImageFile(context as AppCompatActivity)
                photoFile.copyBitmapToFile(rotatedBitmap!!)
                profileModel.photoFile.value = photoFile
                val photoURI: Uri = FileProvider.getUriForFile(
                    context as AppCompatActivity,
                    "com.example.lab2_final.fileprovider",
                    profileModel.photoFile.value!!
                )
                profileModel.updateUser(edit_fullname.text.toString(),
                    edit_nickname.text.toString(),
                    edit_email.text.toString(),
                    edit_geoArea.text.toString(),
                    edit_telephone.text.toString(),
                    edit_password.text.toString(),
                    photoURI)

                findNavController()
                    .navigate(R.id.action_nav_edit_profile_to_nav_show_profile)
                printSuccessMessage(requireView(), requireContext())
                return true
            }
            R.id.cancel -> {
                findNavController()
                    .navigate(R.id.action_nav_edit_profile_to_nav_show_profile)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }


    /*  dispatchTakePictureIntent
    *
    *  This method is invoked to create the intent and dispatch it to the
    *  camera in order to shot the photo.
    *
    */
    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity((context as AppCompatActivity).packageManager)?.also {
                val photoFile =
                    createImageFile(context as AppCompatActivity)
                profileModel.photoFile.value = photoFile
                val photoURI: Uri = FileProvider.getUriForFile(
                    context as AppCompatActivity,
                    "com.example.lab2_final.fileprovider",
                    profileModel.photoFile.value!!
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
            }
        }
    }


    /*  onActivityResult
     *
     *  This method is called whenever a photo is shot from camera or uploaded
     *  from the gallery.
     *
     *  N.B !! If the photo is uploaded from the gallery it is needed to set
     *         currentPhotoPath.
     *         This is not necessary if the photo is shot from camera because
     *         the file that will store the photo is created before the photo
     *         is taken, so the currentPhotoPath is already set.
     *
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {


        } else if (requestCode == REQUEST_LOAD_PHOTO && resultCode == RESULT_OK){
            /* save the photo in our internal storage (FilesDir), in this way if the
               user delete it from gallery it will available for the application anyway
            */

            val destinationFile = createImageFile(context as AppCompatActivity)
            val picToSave = data?.data as Uri
            val inputStream = (context as AppCompatActivity)
                .contentResolver.openInputStream(picToSave) as InputStream

            destinationFile.copyInputStreamToFile(inputStream)

            profileModel.photoFile.value = destinationFile
        } else if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_CANCELED){
            return
        } else if (requestCode == REQUEST_LOAD_PHOTO && resultCode == RESULT_CANCELED){
            return
        }

        GlobalScope.launch {
            compressAndLoad()
        }
        Picasso.get().load(profileModel.photoFile.value!!).placeholder(R.drawable.profile_demo)
            .into(edit_profilePic)
    }

    private suspend fun compressAndLoad() {
        try {
            Compressor.compress(context as AppCompatActivity, profileModel.photoFile.value!!) {
                quality(30)
                default()
                destination(profileModel.photoFile.value!!)
            }
        } catch (e: IOException) {
            Log.d("compressAndLoad", "Compress and load in EditProfileFragment Failed.")
        }
    }


    private fun initializeListenerGUI(){

        registerForContextMenu(camera_button)
        camera_button.setOnClickListener { camera_button.showContextMenu() }

        btn_rotate.setOnClickListener {
            profileModel.numRotation += 1f
            Picasso.get().load(profileModel.photoFile.value!!).rotate(90f*profileModel.numRotation).placeholder(R.drawable.profile_demo).into(edit_profilePic)
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
       /*mapView!!.onSaveInstanceState(outState)*/

        outState.putBoolean("initialized",initialized)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
            super.onCreate(savedInstanceState)
            try {
                MapsInitializer.initialize(getActivity())
            } catch (e: GooglePlayServicesNotAvailableException) {
                mapsSupported = false
            }
            if (mapView != null) {
                mapView!!.onCreate(savedInstanceState)
            }
            initializeMap()

        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
        requireActivity().findViewById<NavigationView>(R.id.nav_view).setCheckedItem(R.id.nav_show_profile)
        if (savedInstanceState!=null)
            initialized = savedInstanceState.getBoolean("initialized")
    }




    override fun onResume() {
        super.onResume()
        mapView!!.onResume()
        initializeMap()
    }

    override fun onMapReady(p0: GoogleMap?) {
        googleMap=p0

        var address: Address? = null
        var addressList: List<Address>? = null
        googleMap?.setOnMapClickListener(GoogleMap.OnMapClickListener { point ->
            googleMap?.clear()
            googleMap?.addMarker(MarkerOptions().position(point))
            googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom((point),15.0F))

            val geocoder = Geocoder(requireActivity())
            try {
                addressList = geocoder.getFromLocation(point.latitude, point.longitude, 1)
                address = addressList!![0]
                edit_geoArea.setText((address!!.getAddressLine(0)) +" " +address!!.locality + " " + " " + address!!.adminArea)


            } catch (e: Exception) {
                e.printStackTrace()
                MaterialAlertDialogBuilder(context)
                    .setTitle("Location")
                    .setMessage("Try again to insert the right address")
                    .setPositiveButton("ok") { dialog, which ->
                        return@setPositiveButton;
                    }
                    .show()
            }

        })
    val location = edit_geoArea.text.toString()
    if (location != null || location != "") {
        val geocoder = Geocoder(requireActivity())
        try {
            addressList = geocoder.getFromLocationName(location, 1)
            address = addressList!![0]


        } catch (e: Exception) {
            e.printStackTrace()
            // dialog per dire che l'indirizzo non è stato trovato
            return;
        }
        googleMap!!.clear()
        val latLng = LatLng(address!!.getLatitude(), address!!.getLongitude())
        googleMap?.addMarker(MarkerOptions().position(latLng))
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom((latLng),15.0F))


    }
}
    private fun initializeMap() {
        if (googleMap == null && mapsSupported) {
            mapView = getActivity()?.findViewById(R.id.mapView7)
        }
    }

    fun onSearch() {
        var address: Address? = null
        val location = edit_geoArea.text.toString()
        var addressList: List<Address>? = null
        if (location != null || location != "") {
            val geocoder = Geocoder(requireActivity())
            try {
                addressList = geocoder.getFromLocationName(location, 1)
                address = addressList!![0]
                googleMap!!.clear()
                val latLng = LatLng(address!!.getLatitude(), address!!.getLongitude())
                googleMap?.addMarker(MarkerOptions().position(latLng))
                googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom((latLng),15.0F))

            } catch (e: Exception) {
                e.printStackTrace()
                MaterialAlertDialogBuilder(context)
                    .setTitle("Location")
                    .setMessage("Try again to insert the right address")
                    .setPositiveButton("ok") { dialog, which ->
                        return@setPositiveButton;
                    }
                    .show()
            }
        }
    }
}


