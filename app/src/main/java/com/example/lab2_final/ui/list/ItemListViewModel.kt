package com.example.lab2_final.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lab2_final.objects.Item
import com.example.lab2_final.objects.Notification

class ItemListViewModel : ViewModel(){

    private var items = MutableLiveData<ArrayList<Item>>()
    private var init:Boolean = false
    private var notification = MutableLiveData(Notification())

    fun activateInterestNotification() {
        if (!init) {
            items = ItemListRepository.getInstance().initSnapshotListener(notification)
            init = true
        }
    }

    fun getItemsObservable() : LiveData<ArrayList<Item>> {
        return items
    }

    fun getNotificationObservable():LiveData<Notification>{
        return notification
    }

    fun setNotificationObservable(n:Notification?){
        notification.value = n
    }


}
