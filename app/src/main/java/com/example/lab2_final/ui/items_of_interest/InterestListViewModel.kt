package com.example.lab2_final.ui.items_of_interest

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lab2_final.objects.Item

class InterestListViewModel:ViewModel() {

    private var itemsOfInterest = InterestListRepository.getInstance().getItemsOfInterest()

    fun getItemsObservable() : LiveData<ArrayList<Item>> {
        return itemsOfInterest
    }
}