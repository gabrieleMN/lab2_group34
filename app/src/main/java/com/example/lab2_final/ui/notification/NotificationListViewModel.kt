package com.example.lab2_final.ui.notification

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lab2_final.helpers.addToFrontList
import com.example.lab2_final.helpers.addToList
import com.example.lab2_final.helpers.removeFromList
import com.example.lab2_final.objects.Notification

class NotificationListViewModel : ViewModel() {

    private var notifications = MutableLiveData(ArrayList<Notification>())
    private var init:Boolean = false
    
    fun activateSoldNotifications(){
        if(!init)
            NotificationListRepository.getInstance().activateNotifications(notifications)
        init = true
    }

    fun getNotificationsObservable() : LiveData<ArrayList<Notification>> {
        return notifications
    }

    /* Interest notificaton are managed externally to this VM because they are related to the
    *  list of item of the current logged user. For this reason we don't manage the deletion
    *  of a notification by means of a snapShotLinster.
    *
    *  On the other hand, sold notificaton are not related to the item of the current logged user
    *  so they can be managed here.
    */
    fun addInterestNotification(notification: Notification) {
        if(!notifications.value!!.contains(notification)) {
            notifications.addToFrontList(notification)
        }
    }

    fun readNotificationItemInterest(n:Notification){
        NotificationListRepository.getInstance().readNotificationItemInterest(n.itemId, n.userInterested,
            {alreadySold ->
                    removeLocalNotification(n)
            })
    }

    fun readNotificationItemSold(userId:String, n: Notification){
        NotificationListRepository.getInstance().readNotificationItemSold(userId,n.itemId,
            {alreadySold ->
                    removeLocalNotification(n)
            })
    }

    private fun removeLocalNotification(notification:Notification){
        notifications.removeFromList(notification)
    }



}