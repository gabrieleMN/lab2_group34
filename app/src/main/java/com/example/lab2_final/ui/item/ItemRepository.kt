package com.example.lab2_final.ui.item

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.lab2_final.helpers.getCurrentDate
import com.example.lab2_final.objects.Item
import com.google.common.io.Files.getFileExtension
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class ItemRepository {
    fun realTimeUpdate(itemID:String): MutableLiveData<Item> {
        val db = Firebase.firestore

        val data = MutableLiveData<Item>()
        var itemObj: Item?
        db
        .collection("items")
        .document(itemID)
        .addSnapshotListener {
            snapshot, e ->
                if (e != null) {
                    return@addSnapshotListener}
                if (snapshot != null && snapshot.exists()) {
                    itemObj = snapshot.toObject(Item::class.java)
                    itemObj!!.setItemId(snapshot.id)
                    data.value = itemObj
                }
        }

        db
        .collection("items")
        .document(itemID)
        .collection("emails")
        .addSnapshotListener{ value, _ ->
            for (dc in value!!.documentChanges) {
                val email = dc.document.id
                if(dc.type == DocumentChange.Type.ADDED) {
                    data.value!!.emailInterested.add(email)
                }else if(dc.type == DocumentChange.Type.REMOVED){
                    data.value!!.emailInterested.remove(email)
                }
            }
            data.value = data.value
        }
        return data
    }


    fun updateItem(item:Item, itemID:MutableLiveData<String>, imageUri: Uri, userLoggedEmail: String) {
        val db = Firebase.firestore
        db
        .collection("items")
        .document(itemID.value!!)
        .set(item.toHashMap())
        .addOnSuccessListener {
            //itemID.value=itemID.value
            uploadImage(imageUri, userLoggedEmail, itemID)
        }
        .addOnFailureListener {
        }

    }

    fun addItem(item: Item, imageUri: Uri, userLoggedEmail: String) {
        val db = Firebase.firestore
        db
        .collection("items")
        .add(item.toHashMap())
        .addOnSuccessListener {
            Log.d("Offline","Add item success")
            val itemId: MutableLiveData<String> = MutableLiveData(it.id)
            uploadImage(imageUri, userLoggedEmail, itemId)
        }
        .addOnFailureListener{
            Log.d("Offline","Add item failure")
        }
    }

    /* Every item has a list of user interested in it. With this function we
     * add an interested user to that list.
     */
    fun addPreferenceToItem(itemId: String, emailUserLogged: String) {
        val db = Firebase.firestore

        val data = getCurrentDate()
        val map = mapOf("read" to false,"date" to data )

        //aggiungiamo la mail dell'utente interessato nell'item
        db
        .collection("items")
        .document(itemId)
        .collection("emails")
        .document(emailUserLogged)
        .set( map )
        .addOnSuccessListener {
            addPreferenceToUser(emailUserLogged, itemId)
            notifyUserOwnerOfInterest(emailUserLogged,itemId)
        }
    }

    fun removePreferenceToItem(itemId: String, emailUserLogged: String) {
        //this user is no more interested in this item
        removeUserFromItem(emailUserLogged,itemId)
        //this user wont be notified if item sold
        removeItemFromUser(emailUserLogged,itemId)
    }

    /* Every user has also a reference to every item he is interested in. Better for performance
    *  reason in the management of our notification system.
    *      Remove read flag, here it is not necessary since when a notification will be read it
    *      will be deleted from the db. (DO THIS IN THE LAST LAB, COOULD BE USEFULL LATER).
    *
    *      On the other hand notification regarding the list of interested user of an item need
    *      a read flag since that entry cannot be deleted when the notification is read bu only
    *      when the corrispondet item is sold.
    */
    fun addPreferenceToUser(userMail: String, itemId: String) {
        val db = Firebase.firestore

        val hmap = hashMapOf<String, Boolean>("read" to false,"sold" to false,"imBuyer" to false)

        db
        .collection("utenti")
        .document(userMail)
        .collection("items")
        .document(itemId)
        .set(hmap)
    }



    /* This function simply set the item field sold = true */
    fun markItemAsSold(itemID :MutableLiveData<String>, buyerEmail :String){
        val db = Firebase.firestore
        val hmap = hashMapOf(
            "sold" to true,
            "buyer" to buyerEmail)

        db
        .collection("items")
        .document(itemID.value!!)
        .update(hmap as Map<String, Any>)
        .addOnSuccessListener { itemID.value = itemID.value }
        .addOnFailureListener { Log.d("sold", it.message.toString()) }

        getInterestedUser(itemID.value!!, buyerEmail)
        
        db
            .collection("utenti")
            .document(Firebase.auth.currentUser!!.email!!)
            .update("soldCounter", FieldValue.increment(1));
    }

    /* This manages the notification of all the users that were interested in the item that has
     * just been sold.
     * First we set the sold=true in every entry of items inside user, next we also need to remove
     * every user interested from the item itself since it has been sold.
     */
    private fun getInterestedUser(itemID:String, buyerEmail: String){
        val db = Firebase.firestore

        db
        .collection("items")
        .document(itemID)
        .collection("emails")
        .get()
        .addOnSuccessListener { documents ->
            for (document in documents) {
                val interestedUser = document.id
                if(interestedUser!=null) {

                    if (interestedUser == buyerEmail) {
                        notifyBuyerItemSold(buyerEmail, itemID)
                        val msg = "You purchased an item!"
                        sendPushNotificationTo(buyerEmail,msg)
                    }
                    else {
                        /*This will delete the reference utenti (collection) -> user_istance -> items (subCoollection) -> item_instance */
                        notifyUserItemSold(interestedUser, itemID)
                        val msg = "An item you've been interested has been sold!"
                        sendPushNotificationTo(interestedUser,msg)
                    }

                    val interestedUser = interestedUser //TODO fratellì, guarda riga 163
                    /*This will delete the reference items (collection) -> item -> emails (subCoollection) -> email_instance */
                    removeUserFromItem(interestedUser, itemID)
                }
            }
        }
        .addOnFailureListener {
            Log.d("sold", it.message.toString())
        }
    }

    /* This will notify the user that the item he was interested of was sold. By setting sold=true
     * will notify the user that an item it was intetested in has been sold.
     */
    private fun notifyUserItemSold(email :String, itemId :String){
        val db = Firebase.firestore

        val data = getCurrentDate()
        val map = mapOf(
            "sold" to true,
            "date" to data
        )

        db
        .collection("utenti")
        .document(email)
        .collection("items")
        .document(itemId)
        .update(map as Map<String, Any>)
        .addOnSuccessListener {  Log.d("sold", "REMOVED $itemId FROM USER $email") }
        .addOnFailureListener { e -> Log.w("DBG", "Error deleting document", e) }

    }

    private fun notifyBuyerItemSold(buyerEmail :String, itemId :String) {
        val db = Firebase.firestore

        val data = getCurrentDate()
        val hmap = hashMapOf (
            "sold" to true,
            "imBuyer" to true,
            "date" to data
        )

        db
        .collection("utenti")

        .document(buyerEmail)
        .collection("items")
        .document(itemId)
        .update(hmap as Map<String, Any>)

    }

    /* If the item was sold it can't have a list of interested user, so delete every user after it
     * was sold.
     */
    private fun removeUserFromItem(user:String,itemId:String){
        val db = Firebase.firestore
        db
            .collection("items")
            .document(itemId)
            .collection("emails")
            .document(user)
            .delete()
            .addOnSuccessListener { Log.d("DBG", "DocumentSnapshot successfully deleted!") }
            .addOnFailureListener { e -> Log.w("DBG", "Error deleting document", e) }
    }

    private fun removeItemFromUser(user:String,itemId:String){
        val db = Firebase.firestore
        db
        .collection("utenti")
        .document(user)
        .collection("items")
        .document(itemId)
        .delete()
        .addOnSuccessListener { Log.d("DBG", "DocumentSnapshot successfully deleted!") }
        .addOnFailureListener { e -> Log.w("DBG", "Error deleting document", e) }
    }

    fun uploadImage(imageUri: Uri, userLoggedEmail: String, itemId: MutableLiveData<String>) {
        val imageReference = FirebaseStorage.getInstance().reference.child("images").child(userLoggedEmail).child("items")
            .child(itemId.value!!)
        val fileRef = imageReference.child("itemPic" + "." + getFileExtension(imageUri.toString()))
        val uploadTask = fileRef.putFile(imageUri)

        val urlTask = uploadTask.continueWithTask { task ->
            if(!task.isSuccessful) {
                task.exception?.let { throw it }
            }
            fileRef.downloadUrl
        }.addOnCompleteListener { task ->
            if(task.isSuccessful){
                Log.d("URL", task.result.toString())
                val imgDownloadUrl = task.result.toString()

                val db = Firebase.firestore
                db.collection("items")
                .document(itemId.value!!)
                .update("imageUrlDownlaod", imgDownloadUrl)
                .addOnSuccessListener {
                    itemId.value = itemId.value
                }
            }
        }
    }

    fun initImageIntoFile(): MutableLiveData<File>{
        val dataFile = MutableLiveData<File>()
        val imageReference = FirebaseStorage.getInstance().reference.child("images")
        val imgRef = imageReference.child("placeholder.png")
        val file = createTempFile("img_init","png")

        imgRef.getFile(file).addOnSuccessListener {
            dataFile.value = file
        }
        return dataFile
    }

    fun loadImageIntoFile(itemId: String,userEmail: String, currentPhotoFile: MutableLiveData<File>){
        val imageReference = FirebaseStorage.getInstance().reference.child("images").child(userEmail).child("items").child(itemId)
        val fileRef = imageReference.child("itemPic" + "." + "jpg")
        val file = createTempFile("img","jpg")

        fileRef.getFile(file).addOnSuccessListener {
            // Local temp file has been created
            Log.d("PhotoFile","Success")
            currentPhotoFile.value = file
        }.addOnFailureListener {
            // Handle any errors
            Log.d("PhotoFile","Failure")
        }

    }

    fun reviewed(itemId: String) {
        val db = Firebase.firestore
        val hmap = hashMapOf("reviewed" to true)

        db
        .collection("items")
        .document(itemId)
        .update(hmap as Map<String, Any>)
    }

    fun notifyUserOwnerOfInterest(interestedEmail: String, itemId: String){
        val db = Firebase.firestore
        db.collection("items")
            .document(itemId)
            .get()
            .addOnSuccessListener {
                val userOwner = it.getString("userOwner")
                Log.d("notification",userOwner)
                val msg = interestedEmail + " is interested in one of your items!"
                sendPushNotificationTo(userOwner!!,msg)
            }
    }

    fun sendPushNotificationTo(userEmail:String, msg: String){
        val db = Firebase.firestore
        db.collection("utenti")
            .document(userEmail)
            .get()
            .addOnSuccessListener {
                val token = it.getString("token")
                Log.d("notification",token)
                val data = hashMapOf(
                        "token" to token,
                        "msg" to msg
                )
                FirebaseFunctions.getInstance().getHttpsCallable("notifyDevice").call(data)
            }
    }

    fun getOwnerImg(itemOwner:String) : LiveData<String>{
        val userImg = MutableLiveData<String>()
        val db = Firebase.firestore
        db.collection("utenti")
            .document(itemOwner)
            .get()
            .addOnSuccessListener {
                userImg.value = it.data?.get("imageUrlDownload") as String
            }
        return userImg
    }

    companion object {
        private var INSTANCE: ItemRepository? = null
        fun getInstance() = INSTANCE
            ?: ItemRepository().also {
                INSTANCE = it
            }
    }


}