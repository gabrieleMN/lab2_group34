package com.example.lab2_final.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lab2_final.R
import com.example.lab2_final.ui.item.ItemViewModel
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_list_fragment.*


class ItemListFragment : Fragment() {

    private lateinit var itemAdapter: ItemAdapter
    private lateinit var itemViewModel: ItemViewModel
    private lateinit var itemListModel: ItemListViewModel

    override fun onStart() {
        super.onStart()
        itemListModel.getItemsObservable().observe(viewLifecycleOwner, Observer { arrayList ->
            itemAdapter.items = arrayList
            itemAdapter.notifyDataSetChanged()

            if(itemListModel.getItemsObservable().value!!.size == 0){
                empty_list.visibility = View.VISIBLE
                listTitle.visibility = View.GONE
            }
            else {
                empty_list.visibility = View.GONE
                listTitle.visibility = View.VISIBLE
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        itemListModel = ViewModelProvider(requireActivity()).get(ItemListViewModel::class.java)
        itemViewModel = ViewModelProvider(requireActivity()).get(ItemViewModel::class.java)
        itemAdapter = ItemAdapter(itemListModel.getItemsObservable().value!!, itemViewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.item_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //set the adapter and layoutManager to the recycler view
        recycler_view.adapter = itemAdapter

        //set the layout manager, should only be set once
        recycler_view.layoutManager = LinearLayoutManager(context)

        //set the onclicklistener to the FAB for adding new item
        btn_addItem.setOnClickListener {
            val bundle = Bundle()
            bundle.putBoolean("Item.addNew", true)
            findNavController().navigate(R.id.action_nav_item_list_to_itemEditFragment, bundle)
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
        requireActivity().findViewById<NavigationView>(R.id.nav_view).setCheckedItem(R.id.nav_item_list)
    }
}
