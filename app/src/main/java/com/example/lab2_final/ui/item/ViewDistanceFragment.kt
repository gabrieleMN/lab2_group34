package com.example.lab2_final.ui.item

import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels

import com.example.lab2_final.R
import com.example.lab2_final.ui.profile.ProfileViewModel
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions

class ViewDistanceFragment : Fragment(), OnMapReadyCallback{
        private val profileViewModel: ProfileViewModel by activityViewModels()
        private val itemViewModel : ItemViewModel by activityViewModels()

        private var googleMap: GoogleMap? = null
        private var mapView: MapView? = null
        private var mapsSupported = true

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
            super.onCreate(savedInstanceState)
            try {
                MapsInitializer.initialize(getActivity())
            } catch (e: GooglePlayServicesNotAvailableException) {
                mapsSupported = false
            }
            if (mapView != null) {
                mapView!!.onCreate(savedInstanceState)
            }
            initializeMap()
            (activity as? AppCompatActivity)?.supportActionBar?.title = ""

        }

        private fun initializeMap() {
            if (googleMap == null && mapsSupported) {
                mapView = getActivity()?.findViewById(R.id.mapView3)
                //setup markers etc...
            }
        }

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View {
            val parent =
                inflater.inflate(R.layout.fragment_view_distance, container, false)
            mapView = parent.findViewById(R.id.mapView3) as MapView
            mapView!!.getMapAsync(this)
            return parent
        }

        override fun onSaveInstanceState(outState: Bundle) {
            super.onSaveInstanceState(outState)
            mapView!!.onSaveInstanceState(outState)
        }

        override fun onResume() {
            super.onResume()
            mapView!!.onResume()
            initializeMap()
        }

        override fun onPause() {
            super.onPause()
            mapView!!.onPause()
        }

        override fun onDestroy() {
            super.onDestroy()
            mapView!!.onDestroy()
        }

        override fun onLowMemory() {
            super.onLowMemory()
            mapView!!.onLowMemory()
        }

        override fun onMapReady(p0: GoogleMap?) {
            googleMap=p0
            val locationUser = profileViewModel.loggedUserGeoA.value!!.toString()
            val locationItem=itemViewModel.locationVM.value!!
            var address: Address? =null
            var address2: Address? =null

            var addressList: List<Address>? = null

            if (locationUser != null || locationUser != "" || locationItem != null || locationItem != "") {
                val geocoder = Geocoder(requireActivity())
                try {
                    addressList = geocoder.getFromLocationName(locationUser, 1)
                    address = addressList!![0]

                    addressList = geocoder.getFromLocationName(locationItem, 1)
                    address2 = addressList!![0]


                } catch (e: Exception) {
                    e.printStackTrace()
                    // dialog per dire che l'indirizzo non è stato trovato
                    return;
                }
                val latLng = LatLng(address!!.getLatitude(), address!!.getLongitude())
                val latLng2 = LatLng(address2!!.getLatitude(), address2!!.getLongitude())

                val line = googleMap!!.addPolyline(
                    PolylineOptions().add(
                        latLng,
                        latLng2
                    ).width(2F).color(Color.BLUE).geodesic(true)
                )
                googleMap?.addMarker(MarkerOptions().position(latLng))
                googleMap?.addMarker(MarkerOptions().position(latLng2))
                googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng2,4.0F))
            }


        }
}
