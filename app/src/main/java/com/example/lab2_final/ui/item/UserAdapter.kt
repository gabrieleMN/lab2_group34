package com.example.lab2_final.ui.item

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.lab2_final.R
import com.example.lab2_final.objects.User
import com.example.lab2_final.ui.profile.ProfileViewModel
import com.squareup.picasso.Picasso

class UserAdapter(var items:ArrayList<User>, var profileViewModel: ProfileViewModel): RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    val profileVM: ProfileViewModel = profileViewModel

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserAdapter.ViewHolder {
        val v= LayoutInflater.from(parent.context)
            .inflate(R.layout.user_interested_card_layout, parent, false)
        return UserAdapter.ViewHolder(v, profileViewModel)
    }

    override fun onBindViewHolder(holder: UserAdapter.ViewHolder, position: Int) {
        holder.bind(items[position])
    }
    override fun onViewRecycled(holder: ViewHolder) {
        holder.unbind()
    }

    class ViewHolder(v: View, profileViewModel: ProfileViewModel):RecyclerView.ViewHolder(v) {
        val profileVM = profileViewModel
        val user: ConstraintLayout = v.findViewById(R.id.user)
        val name: TextView = v.findViewById(R.id.user_nickname)
        val photo: ImageView = v.findViewById(R.id.user_img)
        fun bind(i: User) {
            name.text = i.nickname
            //photo.setImageURI(Uri.parse(i.imagePath))
            user.setOnClickListener {
                profileVM.loadUserInViewModel(i.email)
                //Navigation.findNavController(it).navigate(R.id.action_nav_item_details_to_nav_show_profile_other)
                Navigation.findNavController(it).navigate(R.id.action_nav_item_details_to_nav_show_profile)
            }
            Picasso.get().load(i.imageUrlDownload).into(photo)
        }
        fun unbind(){
            user.setOnClickListener(null)
        }

    }

}