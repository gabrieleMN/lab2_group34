package  com.example.lab2_final.ui.item


import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lab2_final.R
import com.example.lab2_final.objects.User
import com.example.lab2_final.ui.profile.ProfileViewModel
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso
import com.stepstone.apprating.AppRatingDialog
import kotlinx.android.synthetic.main.item_details_fragment.*
import kotlin.collections.ArrayList

class ItemDetailsFragment : Fragment(), OnMapReadyCallback{

    private lateinit var userAdapter: UserAdapter

    private val itemViewModel : ItemViewModel by activityViewModels()
    private val profileViewModel: ProfileViewModel by activityViewModels()

    private var userList: ArrayList<User> = ArrayList()
    private var interest: Boolean = false
    private var own: Boolean = false
    var googleMap: GoogleMap? = null
    var mapView: MapView? = null
    private var mapsSupported = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userAdapter = UserAdapter(userList, profileViewModel)
    }

    override fun onStart() {
        super.onStart()
        registerObserversRealTimeItemDetails(itemViewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (requireArguments().getBoolean("showInterestedUsers"))
        setHasOptionsMenu(true)
        val parent =
            inflater.inflate(R.layout.item_details_fragment, container, false)
        mapView = parent.findViewById(R.id.mapView2) as MapView
        mapView!!.getMapAsync(this)
        return parent    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        user_recycler_view.adapter = userAdapter
        user_recycler_view.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.HORIZONTAL,
            false)

        btn_sold.setOnClickListener{
            //TODO navigate verso il fragment della soldItemInterestedUsers
            findNavController().navigate(R.id.action_nav_item_details_to_soldItemInterestedUsers)
            //itemViewModel.markItemAsSold()
        }

        btn_interest.setOnClickListener {

            if(interest){
                itemViewModel.addPreferenceToItem(profileViewModel.getEmailUserLogged().value!!)
                btn_interest.drawable.mutate()
                    .setTint(ContextCompat.getColor(requireActivity(),R.color.red_700))

                interest = false
            }else{
                itemViewModel.removePreferenceToItem(profileViewModel.getEmailUserLogged().value!!)
                btn_interest.drawable.mutate()
                    .setTint(ContextCompat.getColor(requireActivity(),R.color.white))
                interest = true
            }

        }

        btn_rating.setOnClickListener {

            AppRatingDialog.Builder()
            .setPositiveButtonText("Submit")
            .setNegativeButtonText("Cancel")
            .setNoteDescriptions(listOf("Very Bad", "Not good", "Quite ok", "Very Good", "Excellent !!!"))
            .setDefaultRating(2)
            .setTitle("Rate this seller")
            .setDescription("how was the product sale?")
            .setCommentInputEnabled(true)
            .setStarColor(R.color.starColor)
            .setNoteDescriptionTextColor(R.color.noteDescriptionTextColor)
            .setTitleTextColor(R.color.titleTextColor)
            .setDescriptionTextColor(R.color.contentTextColor)
            .setHint("Leave a comment about the seller...")
            .setHintTextColor(R.color.hintTextColor)
            .setCommentTextColor(R.color.commentTextColor)
            .setCommentBackgroundColor(R.color.grey_200)
            .setWindowAnimation(R.style.MyDialogFadeAnimation)
            .setCancelable(false)
            .setCanceledOnTouchOutside(false)
            .create(requireActivity())
            .show()

        }

        if (!requireArguments().getBoolean("showInterestedUsers")){
            divider4.visibility = View.GONE
            user_recycler_view.visibility = View.GONE
            edit_item_interested_users.visibility = View.GONE
            btn_sold.visibility = View.GONE
            textViewSold.visibility = View.GONE
            btn_interest.visibility = View.VISIBLE
            own = false
        }else{

            /* If interested users are shown it means that i am the owner of the item so i must no
            *  be able to express my interest in one of my item */
            btn_interest.visibility = View.GONE
            own = true
        }

        btn_rating.visibility = View.GONE
        val searchView: SearchView = requireActivity().findViewById(R.id.searchView)
        searchView.visibility = View.GONE
        val filtersMenu: ConstraintLayout = requireActivity().findViewById(R.id.filtersMenu)
        filtersMenu.visibility = View.GONE

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.option_menu_item_details, menu)
        if (itemViewModel.isSold.value != null && itemViewModel.isSold.value!!){
            menu.removeItem(R.id.edit_fragment)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.edit_fragment -> {
                findNavController().navigate(R.id.action_nav_item_details_to_itemEditFragment)
                return true
            }
            android.R.id.home -> {
                findNavController().navigate(R.id.action_nav_item_details_to_nav_item_list)
                return true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    fun registerObserversRealTimeItemDetails(detailsVM: ItemViewModel){

        /* Change the view accordingly to real time changes */
        detailsVM.nameVM.observe(viewLifecycleOwner, Observer { item_name -> this.item_name.text = item_name })
        detailsVM.cateogoryVM.observe(viewLifecycleOwner, Observer { category-> this.item_category.text = category })
        detailsVM.cateogoryVM.observe(viewLifecycleOwner, Observer { category-> this.chip3.text = category })
        detailsVM.subcategoryVM.observe(viewLifecycleOwner, Observer { subcategory -> this.chip4.text = subcategory })
        detailsVM.priceVM.observe(viewLifecycleOwner, Observer { price -> this.item_price.text = price })
        detailsVM.descriptionVM.observe(viewLifecycleOwner, Observer { description -> this.item_description.text = description })
        detailsVM.locationVM.observe(viewLifecycleOwner, Observer { location -> this.item_address.text = location
            mapView!!.getMapAsync(this)

        })
        detailsVM.cityVM.observe(viewLifecycleOwner, Observer { city -> this.item_geoArea.text = city })
        detailsVM.expiryDateVM.observe(viewLifecycleOwner, Observer { date -> this.item_expiry.text = date })
        detailsVM.creationDateVM.observe(viewLifecycleOwner, Observer { creation_date -> this.item_date.text = creation_date })
        detailsVM.itemOwner.observe(viewLifecycleOwner, Observer { userId ->
            if (!profileViewModel.isUserLogged(userId)) {
                btn_sold.visibility = View.GONE
                this.ownerMail.text = userId
                ownerMail.setOnClickListener {
                    profileViewModel.loadUserInViewModel(userId)
                    findNavController().navigate(R.id.action_nav_item_details_to_nav_show_profile2)
                }
                ownerImg.setOnClickListener {
                    profileViewModel.loadUserInViewModel(userId)
                    findNavController().navigate(R.id.action_nav_item_details_to_nav_show_profile2)
                }
                divider9.visibility = View.VISIBLE
                item_owner_label.visibility = View.VISIBLE
                ownerMail.visibility = View.VISIBLE
                ownerImg.visibility = View.VISIBLE
            } else{
                divider9.visibility = View.GONE
                item_owner_label.visibility = View.GONE
                ownerMail.visibility = View.GONE
                ownerImg.visibility = View.GONE
            }
        })
        detailsVM.itemOwnerImg.observe(viewLifecycleOwner, Observer{
            if (it!="")
                Picasso.get().load(it).placeholder(R.drawable.placeholder).into(ownerImg)
        })
        detailsVM.reviewed.observe(viewLifecycleOwner, Observer { reviewed ->
            if (reviewed)
                btn_rating.visibility = View.GONE
        })

        detailsVM.imgDownloadUrl.observe(viewLifecycleOwner, Observer { imgDownloadUrl ->
            Picasso.get().load(imgDownloadUrl).placeholder(R.drawable.placeholder).into(item_pic)
        })

        /* Manage real time changes on interested user list */
        detailsVM.userInterestedVM.observe(viewLifecycleOwner, Observer { listUser ->
            userAdapter.items = listUser
            userAdapter.notifyDataSetChanged()

            checkInterest(listUser)
        })

        /* Change item view if the item was sold */
        detailsVM.isSold.observe(viewLifecycleOwner, Observer{ isSold ->
            if (!isSold){
                textViewSold.visibility = View.GONE
                btn_rating.visibility = View.GONE
                if (own) {
                    divider4.visibility = View.VISIBLE
                    edit_item_interested_users.visibility = View.VISIBLE
                    user_recycler_view.visibility = View.VISIBLE
                    requireActivity().invalidateOptionsMenu()
                    btn_sold.isEnabled = true
                    btn_sold.visibility = View.VISIBLE
                }else{
                    btn_interest.visibility=View.VISIBLE
                    btn_sold.visibility = View.GONE
                }

            } else{
                textViewSold.visibility = View.VISIBLE
                btn_sold.visibility = View.GONE
                divider4.visibility = View.GONE
                edit_item_interested_users.visibility = View.GONE
                user_recycler_view.visibility = View.GONE
                requireActivity().invalidateOptionsMenu()
                btn_interest.visibility=View.GONE

                if (itemViewModel.buyerEmail.value != null)
                    if (detailsVM.reviewed.value != null
                        && !detailsVM.reviewed.value!!
                        && itemViewModel.itemOwner.value!! != profileViewModel.loggedUserEmail.value!!      // if i'm not the owner - we can safe delete this
                        && itemViewModel.buyerEmail.value!! == profileViewModel.loggedUserEmail.value!!)    // if i'm the buyer
                            btn_rating.visibility = View.VISIBLE
            }
        })

        detailsVM.buyerEmail.observe(viewLifecycleOwner, Observer { buyerEmail ->
            if (buyerEmail != "")
                if (detailsVM.reviewed.value != null
                    && !detailsVM.reviewed.value!!
                    && itemViewModel.itemOwner.value!! != profileViewModel.loggedUserEmail.value!!      // if i'm not the owner - we can safe delete this
                    && buyerEmail == profileViewModel.loggedUserEmail.value!!)                          // if i'm the buyer
                    btn_rating.visibility = View.VISIBLE
        })

    }

    private fun checkInterest(userList:List<User>){
        val loggedUser = profileViewModel.getEmailUserLogged().value!!
        var preference:Boolean = false
        for(user in userList){
            if(user.email == loggedUser){
                preference = true
                break
            }
        }
        if(preference){
            interest = false
            btn_interest.drawable.mutate()
                .setTint(ContextCompat.getColor(requireActivity(), R.color.red_700))
        }
        else {
            interest = true
            btn_interest.drawable.mutate()
                .setTint(ContextCompat.getColor(requireActivity(), R.color.white))
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        super.onCreate(savedInstanceState)
        try {
            MapsInitializer.initialize(getActivity())
        } catch (e: GooglePlayServicesNotAvailableException) {
            mapsSupported = false
        }
        if (mapView != null) {
            mapView!!.onCreate(savedInstanceState)
        }
        initializeMap()
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""
    }
    override fun onResume() {
        super.onResume()
        mapView!!.onResume()
        initializeMap()
    }

    override fun onMapReady(p0: GoogleMap?) {
        googleMap=p0


        var address: Address? =null
        val location = item_address.text.toString()
        var addressList: List<Address>? = null
        if (location != null || location != "") {
            val geocoder = Geocoder(requireActivity())
            try {
                addressList = geocoder.getFromLocationName(location, 1)
                address = addressList!![0]

            } catch (e: Exception) {
                e.printStackTrace()
                // dialog per dire che l'indirizzo non è stato trovato
                return;
            }
            val latLng = LatLng(address!!.latitude, address.longitude)
            googleMap!!.addMarker(MarkerOptions().position(latLng))
            googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15.0F))
        }

        if (!requireArguments().getBoolean("showInterestedUsers")) {
            googleMap?.setOnMapClickListener{
                findNavController().navigate(R.id.action_nav_item_details_to_ViewDistanceFragment)

            }
        }
    }

    private fun initializeMap() {
        if (googleMap == null && mapsSupported) {
            mapView = getActivity()?.findViewById(R.id.mapView2)
        }
    }

}
