package com.example.lab2_final.ui.profile

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.lab2_final.objects.User
import java.io.File

class ProfileViewModel : ViewModel() {

    //Private Mutable Live Data to update the state of view model with changes from repo
    private var userId: MutableLiveData<String> = MutableLiveData("sandromarelli@icloud.com")
    private val userVM: LiveData<User> = Transformations.switchMap(userId) {id -> ProfileRepository.getInstance().getUser(id)}

    private val flagUserExist: MutableLiveData<Boolean> = MutableLiveData(false)
    private val flagLoginFailure: MutableLiveData<Boolean> = MutableLiveData(false)
    private var emailUserLogged: MutableLiveData<String> = MutableLiveData("")
    private val userLogged: LiveData<User> = Transformations.switchMap(emailUserLogged) {id ->
        if (id!="")
            ProfileRepository.getInstance().getUser(id)
        else
            MutableLiveData(User())
    }

    val loggedUserFullname: LiveData<String> = Transformations.map(userLogged){user -> user.fullname}
    val loggedUserEmail: LiveData<String> = Transformations.map(userLogged){user -> user.email}
    val loggedUserGeoArea: LiveData<String> = Transformations.map(userLogged){user -> user.geoArea}
    var loggedUserGeoA:MutableLiveData<String> = MutableLiveData("")


    //Public (via getters) Live Data to offer observers to fragments
    val userNameVM: LiveData<String> = Transformations.map(userVM){ user -> user.fullname }
    val userNicknameVM: LiveData<String> = Transformations.map(userVM){ user -> user.nickname }
    val userEmailVM: LiveData<String> = Transformations.map(userVM){ user -> user.email }
    val userGeoAreaVM: LiveData<String> = Transformations.map(userVM){ user -> user.geoArea }
    val userTelephoneVM: LiveData<String> = Transformations.map(userVM){ user -> user.telephone }
    val userPasswordVM: LiveData<String> = Transformations.map(userVM){ user -> user.password }
    val userRateReviewTot: LiveData<Number> = Transformations.map(userVM){ user -> user.rateReviewTot }
    val userNReviews: LiveData<Number> = Transformations.map(userVM){ user -> user.nReviews }
    val userSoldCounter: LiveData<Number> = Transformations.map(userVM){ user -> user.soldCounter }

    val imgDownloadUrlUserLogged : LiveData<String> = Transformations.map(userLogged){ userLogged -> userLogged.imageUrlDownload }
    val imgDownloadUrl: LiveData<String> = Transformations.map(userVM){user -> user.imageUrlDownload}
    var photoFile: MutableLiveData<File> = ProfileRepository.getInstance().initImageIntoFile()
    var numRotation = 0f


    fun getEmailUserLogged(): LiveData<String> {
        return emailUserLogged
    }


    fun getFlagUserExist(): LiveData<Boolean> {
        return flagUserExist
    }

    fun getFlagLoginFailure(): LiveData<Boolean> {
        return flagLoginFailure
    }

    fun loadUserInViewModel(userId:String){
        this.userId.value = userId
    }

    fun isCurrentUser(): Boolean{
        return userId.value == emailUserLogged.value
    }

    fun isUserLogged(userId: String): Boolean{
        return userId == emailUserLogged.value
    }

    fun updateUser(fullname: String, nickname: String, email: String,
                   geoArea: String,telephone:String,
                   password: String,photoUri: Uri) {

        val usr = User(fullname, nickname, email, geoArea, telephone,password)
        ProfileRepository.getInstance().updateUser(usr, userId, emailUserLogged,photoUri)
    }

    fun login(mail: String) {
        ProfileRepository.getInstance().login(mail, emailUserLogged, flagLoginFailure)
    }

    fun register(mail: String, pwd: String) {
        ProfileRepository.getInstance().register(mail, pwd, emailUserLogged, flagUserExist)
    }

    fun getFileFromRepository() {
        ProfileRepository.getInstance().loadImageIntoFile(userEmailVM.value!!,photoFile)
    }

    fun addReview(itemReviewed :String, rate :Int, comment :String, sellerEmail :String) {
        ProfileRepository.getInstance().addReview(itemReviewed, emailUserLogged.value!!, userLogged.value!!.nickname, rate, comment, sellerEmail)
    }
}