package com.example.lab2_final.ui.review

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lab2_final.objects.Review


class ReviewViewModel : ViewModel() {


    private var reviews = MutableLiveData<ArrayList<Review>>()

    fun loadReviews(userEmail :String) {
        reviews = ReviewRepository.getInstance().getReviews(userEmail)
    }

    fun getReviewsArrayList() :ArrayList<Review>{
        return reviews.value!!
    }
}